#!/usr/bin/env node

/** Let's try to solve the puzzles
 * @copyright 2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import fs from 'node:fs'
import process from 'node:process'
import { fileURLToPath } from 'node:url'

import { Trax } from '@slugbugblue/trax'
import { suggest } from '@slugbugblue/trax/analyst'
import { puzzles } from '@slugbugblue/trax/puzzles'
import { version } from '@slugbugblue/trax/version'
import { timeString } from '@slugbugblue/trax/utils'

import * as tty from '@slugbugblue/trax/tty'

let STUMPED = false
const FINAL = {}

const record = (name, data) => {
  FINAL['puzzles.firstmove.' + name] = data
}

const time = (ms) => timeString(Date.now() - ms)

const out = (messages) => {
  tty.outty(messages.join(' '))
}

const section = (title) => {
  out([tty.color('===', 250), tty.color(title, 39), tty.color('===', 250)])
}

const success = (puzzle, ms) => {
  out([tty.color('✓', 35), tty.color(puzzle, 250), tty.color(ms, 240)])
}

const puzzled = (puzzle, move) => {
  tty.display(new Trax(puzzle.game, puzzle.notation))
  tty.outty('Puzzled by ' + puzzle.id, 205)
  out([
    tty.color('  played', 250),
    tty.color(move, 127),
    tty.color('instead of', 250),
    tty.color(puzzle.hint || puzzle.hints.join(' '), 39),
  ])
  STUMPED = true
}

const finalize = (name, ms, count) => {
  out([
    tty.color('Finished', 195),
    tty.color(String(count) + ' ' + name + ' puzzles', 123),
    tty.color('in ' + time(ms), 195),
  ])
}

const solvePuzzleLevel = (name, level) => {
  if (STUMPED) return 0
  section(name + ' puzzles')
  name = name.toLowerCase()
  let count = 0
  const begin = Date.now()
  for (const puzzle of puzzles) {
    if (puzzle.level !== level) continue
    const start = Date.now()
    const game = new Trax(puzzle.game, puzzle.notation)
    const { move } = suggest(game).pick
    const solved = puzzle.hint
      ? move === puzzle.hint
      : puzzle.hints
      ? puzzle.hints.includes(move)
      : false
    if (solved) {
      success(puzzle.id, time(start))
      count++
      record(name, { count, ms: Date.now() - begin })
    } else {
      puzzled(puzzle, move)
      process.exitCode = level + 1
      return count
    }
  }

  finalize(name, begin, count)
  return count
}

// Gather the benchmark data
let total = 0
const timer = Date.now()
total += solvePuzzleLevel('Tutorial', 0)
total += solvePuzzleLevel('Beginner', 1)
total += solvePuzzleLevel('Tough', 2)
total += solvePuzzleLevel('Advanced', 3)
total += solvePuzzleLevel('Genius', 4)

record('total', { count: total, ms: Date.now() - timer })
tty.outty(
  'Benchmark: ' + total + ' puzzles in ' + time(timer),
  STUMPED ? 205 : 35,
)

tty.outty()

// Load the previous benchmarks
const FILE = new URL('benchmarks.json', import.meta.url)
let CONTENTS
try {
  CONTENTS = fs.readFileSync(FILE, 'utf8')
} catch {
  CONTENTS = '{}'
}

const BENCHMARK = JSON.parse(CONTENTS)

const PREV = Object.values(BENCHMARK).pop() || {}

// Report the benchmark data
const size = {
  name: 0,
  count: 0,
  countDiff: 0,
  ms: 0,
  avg: 0,
  avgDiff: 0,
  percent: 0,
}

const REPORT = []
for (const name of Object.keys(FINAL)) {
  const metric = FINAL[name]
  const last = PREV[name] || { ms: 0, count: 0 }
  const hasLast = last.count > 0
  const countDiff = metric.count - last.count
  const avg = metric.ms / metric.count
  const lastAvg = hasLast ? last.ms / last.count : 0
  const avgDiff = avg - lastAvg
  const percent = hasLast ? (100 * avgDiff) / lastAvg : 0
  const report = {
    name,
    count: String(metric.count) + ' puzzles',
    countDiff: hasLast ? String(countDiff || '--') : 'new',
    countColor: countDiff > 0 ? 35 : countDiff < 0 ? 208 : hasLast ? 240 : 250,
    ms: 'in ' + timeString(metric.ms),
    avg: 'avg ' + timeString(metric.ms / metric.count),
    avgDiff: hasLast ? timeString(Math.abs(avgDiff)) : 'new',
    avgColor: avgDiff > 0 ? 208 : hasLast ? 35 : 250,
    percent: percent.toFixed(1) + '%',
  }
  if (hasLast) {
    if (avgDiff < 0) {
      report.avgDiff = '-' + report.avgDiff
    } else if (avgDiff > 0) {
      report.avgDiff = '+' + report.avgDiff
    }

    if (countDiff > 0) report.countDiff = '+' + report.countDiff
  }

  for (const key of Object.keys(size)) {
    size[key] = Math.max(size[key], report[key].length)
  }

  REPORT.push(report)
}

for (const line of REPORT) {
  out([
    tty.color(line.name.padEnd(size.name), 39),
    tty.color(line.count.padStart(size.count), 123),
    tty.color(line.countDiff.padStart(size.countDiff), line.countColor),
    tty.color(line.ms.padStart(size.ms), 195),
    tty.color(line.avg.padEnd(size.avg), 245),
    tty.color(line.avgDiff.padStart(size.avgDiff), line.avgColor),
    tty.color(line.percent.padStart(size.percent), line.avgColor),
  ])
}

tty.outty()

// Save the benchmark data to a file
if (BENCHMARK[version]) {
  tty.outty('Not overwriting existing benchmark data')
} else {
  BENCHMARK[version] = FINAL
  try {
    fs.writeFileSync(FILE, JSON.stringify(BENCHMARK, null, 2), 'utf8')
    const filename = fileURLToPath(FILE)
    tty.outty('Saved benchmark data to ' + tty.color(filename, 74))
  } catch (error) {
    tty.outty('Error saving benchmark data', 205)
    tty.outty(String(error.code) + ' ' + error.message, 127)
  }
}
