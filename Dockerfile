# Use the latest nodejs long-term-support release,
# on the latest Alpine Linux for its small footprint
FROM node:lts-alpine
# Install current source code
WORKDIR /trax
COPY . .
RUN npm install
# Ensure the data files can be persisted
WORKDIR /mnt/data
ENV XDG_CONFIG_HOME=/mnt/data
ENV XDG_DATA_HOME=/mnt/data
VOLUME /mnt/data
# Use the trax CLI as the starting point
ENTRYPOINT ["/trax/src/cli.js"]
