/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI suggest command

import { suggest } from '@slugbugblue/trax/analyst'
import { timeString } from '@slugbugblue/trax/utils'

export const suggestCmd = {
  name: 'suggest',
  alt: ['bot'],
  desc: 'suggest a move',
  help: ['Suggest a move in the current game.'],
}

suggestCmd.fn = (CLI, debug) => {
  if (!String(CLI.GAME?.id)) {
    return CLI.error('No active game. Type "new" to start a game.')
  }

  const game = CLI.TRAX
  const suggestion = suggest(game)

  if (!suggestion.best) return

  CLI.out(
    CLI.bubble(
      game.turn === 1 ? 'w' : 'b',
      String(game.move + 1) + '. ' + suggestion.pick.move,
    ),
  )

  if ('debug'.startsWith(debug)) {
    const m = []
    for (const move of suggestion.options) {
      m.push(move.move + ' ' + move.score)
    }

    CLI.out(
      '[' + m.join(', ') + '] ' + CLI.color.black(timeString(suggestion.ms)),
    )
  }
}
