/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI analyze command

import { analyze } from '@slugbugblue/trax/analyst'
import { Trax } from '@slugbugblue/trax'

const notationRx = /^[@a-z]+\d+[bps/\\+]$/i

export const analyzeCmd = {
  name: 'analyze',
  args: '[move]',
  comp: '<play>',
  desc: 'analyze a position',
  help: [
    'Analyze the current position of the currently selected game, or pass in a',
    'move to analyze the result of that move.',
  ],
}

const threatNames = {
  0: 'corners',
  1: 'attacks',
  2: 'Ls',
}

const listThreats = (threats, detailed) => {
  const summary = []
  for (const level of Object.keys(threats || {})) {
    const name = (threatNames[level] || `${level}-stage threats`) + ': '
    const items = []
    if (detailed) {
      for (const item of threats[level]) {
        items.push('[' + item.at + ':' + item.threat + ':' + item.match + ']')
      }
    }

    summary.push(name + threats[level].length + ' ' + items.join(' '))
  }

  return summary.join('  ')
}

const listFaulty = (faulty) => {
  const summary = []
  for (const threat of faulty) {
    summary.push(
      `[L${threat.level} ${threat.at}:${threat.threat}:${threat.match}]`,
    )
  }

  return 'Faulty: ' + summary.join(' ')
}

analyzeCmd.fn = (CLI, move) => {
  if (!String(CLI.GAME?.id)) {
    return CLI.error('No active game. Type "new" to start a game.')
  }

  let trax = CLI.TRAX

  const detailed = 'detail'.startsWith(move) || 'debug'.startsWith(move)

  if (notationRx.test(move)) {
    move = CLI.fixNotation(move)
    CLI.do('try', move)
    trax = new Trax(trax.rules, trax.moves, 'cli')
    const play = trax.dropTile(move)
    if (!play.valid || trax.over) return
  }

  if (trax.over) {
    return CLI.error('Game has ended.')
  }

  const { edge, threats, scores, faulty } = analyze(trax)

  if (detailed) CLI.out(CLI.color.white(edge.w))
  CLI.out(
    CLI.bubble('w', scores.w) +
      CLI.color.white(' ') +
      listThreats(threats.w, detailed),
  )

  if (detailed) {
    if (faulty.w.length > 0) CLI.out(CLI.color.white('') + listFaulty(faulty.w))
    CLI.out(CLI.color.black(edge.b))
  }

  CLI.out(
    CLI.bubble('b', scores.b) +
      CLI.color.black(' ') +
      listThreats(threats.b, detailed),
  )

  if (detailed && faulty.b.length > 0) {
    CLI.out(CLI.color.black('') + listFaulty(faulty.b))
  }
}
