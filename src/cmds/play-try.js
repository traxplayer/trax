/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI play/try commands

import { suggest } from '@slugbugblue/trax/analyst'
import { timeString } from '@slugbugblue/trax/utils'

const moveRx = /^\d+\.?$/
const notationRx = /^[@a-z]+\d+[bps/\\+]$/i

export const playCmd = {
  name: 'play',
  alt: ['move', 'mv'],
  args: '<move> [...]',
  comp: '<play>',
  desc: 'play a move',
  rx: [moveRx, notationRx],
  help: [
    'Enter the move in standard Trax notation. The final character of the',
    'notation can be replaced with a letter for ease of entering at the',
    'command line. Use <s> for "/", <b> for "\\", and <p> for "+".',
    'Multiple moves can be submitted at once. Move numbers are optional,',
    'but if included, they will be checked for accuracy.',
  ],
}

export const tryCmd = {
  name: 'try',
  alt: 'see',
  args: '<move>',
  comp: '<play>',
  desc: 'see the effects of a move',
  help: [
    'View a move without committing to it. This is useful for seeing the',
    'effects of forced tiles.',
  ],
}

const bot = (CLI) => {
  const game = CLI.GAME
  const trax = CLI.TRAX
  const players = game.players || ['a', 'b']
  const name = players[trax.turn - 1]
  if (trax.over && game.puzzle) {
    if (trax.move <= game.max && name !== 'puzzlebot') {
      CLI.puzzleSolved(game.puzzle)
      return 'win'
    }

    return 'lose'
  }

  if (!trax.over && /bot\b/i.test(name)) {
    if (game.puzzle && trax.move >= game.max) {
      trax.play('puzzled')
      return 'lose'
    }

    CLI.do('view')
    CLI.out(CLI.bubble(trax.color + 'd', `${name} thinking...`))
    const start = Date.now()
    const move = suggest(trax)?.pick?.move
    const ms = Date.now() - start
    if (move) {
      CLI.out(
        CLI.bubble(trax.color + 'd', `${name} chooses ${move}`) +
          CLI.color.black(' in ' + timeString(ms)),
      )
      trax.play(move)
    }

    if (game.puzzle) return ' and win by move ' + String(game.max)
  }

  return ''
}

playCmd.fn = (CLI, ...moves) => {
  if (moves.length === 0 || !moves[0]) {
    CLI.error('You must provide a move.')
    return CLI.do('help', 'play')
  }

  if (!CLI.GAME?.id) {
    CLI.do('new')
  }

  const game = CLI.GAME
  const trax = CLI.TRAX

  if (trax.over) {
    return CLI.error('The game is over.')
  }

  const start = trax.move
  let checkMove

  for (let move of moves) {
    if (notationRx.test(move)) {
      const moveNumber = trax.move
      move = CLI.fixNotation(move)
      if (checkMove) {
        trax.play(checkMove, move)
      } else {
        trax.play(move)
      }

      if (trax.move === moveNumber) {
        CLI.error(
          'Move "' +
            (checkMove ? checkMove + '. ' : '') +
            move +
            '" is invalid.',
        )
      }
    } else if (moveRx.test(move)) {
      checkMove = Number(move)
    } else if (move) {
      CLI.error('Move "' + move + '" is not in the correct notation.')
    }
  }

  if (trax.move !== start) {
    const result = bot(CLI)
    CLI.updateGameData()
    CLI.do('view')
    if (result === 'lose') {
      CLI.error('Failed to complete puzzle ' + game.puzzle)
    } else if (result === 'win') {
      CLI.out(CLI.color.success('You completed puzzle ' + game.puzzle))
    } else if (result) {
      CLI.out(result)
    }
  }
}

tryCmd.fn = (CLI, move) => {
  if (!move || move.length === 0) {
    CLI.error('You must provide a move.')
    return CLI.do('help', 'try')
  }

  if (!CLI.TRAX) {
    CLI.do('new')
  }

  if (CLI.TRAX.over) {
    return CLI.error('The game is over.')
  }

  if (notationRx.test(move)) {
    move = CLI.fixNotation(move)
    const play = CLI.TRAX.dropTile(move, undefined, 'tentative')
    if (play.valid || play.dropped.length > 0) {
      CLI.display(CLI.TRAX, CLI.GAME.players, move)
    } else {
      CLI.error('Move is invalid.')
    }
  } else {
    CLI.error('Move is invalid.')
  }
}
