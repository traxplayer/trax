/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI view command

export const viewCmd = {
  name: 'view',
  alt: ['show', 'display'],
  args: '[move number]',
  comp: '<move>',
  desc: 'show the game board',
  help: [
    'Show the current game. Include a move number to see the board as it was at',
    'that move.',
  ],
}

viewCmd.fn = (CLI, move, ...extra) => {
  if (!CLI.GAME?.id) {
    return CLI.error('No active game. Type "new" to start a game.')
  }

  if (move?.length > 1 && 'puzzles'.startsWith(move)) {
    return CLI.do('puzzles', 'list', ...extra)
  }

  const trax = CLI.TRAX

  let show = Number.POSITIVE_INFINITY
  if (move && Number(move) >= 0 && Number(move) <= trax.moves.length) {
    show = Number(move)
  }

  const game = CLI.GAME
  const players = game.players || ['white', 'black']
  const puzzle = game.puzzle || ''
  const { note } = [...(game.notes || [{}])].pop()

  CLI.out(
    CLI.color('#' + CLI.ID + ' ') +
      CLI.name(trax.rules) +
      ' ' +
      CLI.bubble('w' + (trax.turn === 1 ? 'h' : ''), players[0]) +
      CLI.color(' vs ') +
      CLI.bubble('b' + (trax.turn === 2 ? 'h' : ''), players[1]),
  )
  CLI.display(trax, players, undefined, show)
  if (puzzle && (!note || !note.includes(puzzle))) {
    CLI.out(CLI.color('Puzzle ' + puzzle))
  }

  if (note) CLI.out(CLI.color.help(note))
}
