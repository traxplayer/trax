/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI help command

export const helpCmd = {
  name: 'help',
  opts: ['--help', '-h', '-?'],
  alt: '?',
  args: '[topic]',
  comp: '<cmd>',
  desc: 'display information on commands',
  help: [
    'Type "help <command>" or "<command> help" to see additional information',
    'for a specific command.',
  ],
}

// Find the length of the printable characters of the string
const length = (text) => text.replace(/[<">]/g, '').length

helpCmd.fn = (CLI, word) => {
  // If we are looking for help on a particular commands, get that command
  word = CLI.resolveCommand(word) || (word ? String(word).toLowerCase() : '')
  let cmds = CLI.commands.filter((c) => !word || c.startsWith(word))
  if (cmds.length === 0) {
    cmds = CLI.commands.filter((c) => c.includes(word))
  }

  // If we don't have a few commands, select all commands
  if (cmds.length === 0) cmds = CLI.commands

  // Load all the commands we need to print into a local variable
  // and also use the loop to get the length of the longest args
  let max = 1
  const commands = {}
  for (const key of cmds) {
    commands[key] = CLI.cmd(key)
    max = Math.max(max, key.length + length(commands[key].args))
  }

  // Print a summary of all matching commands
  for (const key of cmds.sort()) {
    const topic = commands[key]
    if (topic.hide && !CLI.repl) continue // Hidden commands only in repl
    const space = ' '.repeat(5 + max - length(topic.args) - key.length)
    CLI.out(
      CLI.color.command(`  ${key} ${topic.args}`) +
        CLI.color.short(space + topic.desc),
    )
  }

  // And if we have just one command, print details on that command
  if (cmds.length === 1) {
    const topic = commands[cmds[0]]
    for (const alias of topic.alt) {
      CLI.out(CLI.color.command(`  ${alias}`))
    }

    CLI.out('')
    if (CLI.is(topic.help, 'str')) {
      CLI.out('    ' + CLI.color.help(topic.help))
    } else {
      for (const line of topic.help) {
        CLI.out('    ' + CLI.color.help(line))
      }
    }
  }
}
