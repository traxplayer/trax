/** @file Puzzles CLI
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import { Trax } from '@slugbugblue/trax'
import { levels, puzzles, sources } from '@slugbugblue/trax/puzzles'

let maxLevel = 0
const puzzleIds = []
for (const puzzle of puzzles) {
  maxLevel = Math.max(maxLevel, puzzle.level)
  puzzleIds.push(puzzle.id)
}

const validLevels = levels.slice(0, maxLevel + 1)
const completions = [...validLevels, ...puzzleIds].join('|')

const listAliases = ['display', 'list', 'ls', 'show', 'view']
const startAliases = ['new', 'start']

export const puzzlesCmd = {
  name: 'puzzles',
  args: '["list"|"start"] [level|puzzle]',
  comp: 'display|list|ls|new|show|start|view ' + completions,
  desc: 'view and play puzzles',
  help: [
    'Practice your skills with puzzles. Use "list" to see the puzzle',
    'categories, or "start" to play a puzzle.',
  ],
}

const an = (word = '') => {
  const a = /^[aeiou]/.test(word) ? 'an' : 'a'
  return a + ' ' + word
}

const showPuzzle = (CLI, id, started = false) => {
  let puzzle = puzzles.find((p) => p.id === id)
  if (!puzzle) {
    const possibles = puzzles.filter((p) => p.id.includes(id))
    if (possibles.length === 1) puzzle = possibles[0]
  }

  if (!puzzle) return CLI.error(`Puzzle ${id} not found`)

  const trax = new Trax(puzzle.game, puzzle.notation, 'cli')
  const players = ['white', 'black']
  CLI.display(trax, players)
  let out = CLI.color.short(puzzle.id)
  out += CLI.color(` is ${an(levels[puzzle.level])} puzzle`) + '\n'
  if (puzzle.title) out += CLI.color(puzzle.title) + '\n'
  if (puzzle.desc) out += CLI.color.help(puzzle.desc) + '\n'
  out += CLI.color(players[puzzle.player - 1] + ' to win by move ' + puzzle.max)
  const src = sources[puzzle.src]

  if (src) {
    let source = '\nPuzzle '
    source += src.copyright
      ? 'copyright ' + src.copyright + ' by'
      : 'courtesy of'
    source += ' ' + src.name
    const url = src.url ? ' ' + CLI.color.help(src.url) : ''
    out += CLI.color.white(source) + url
    if (src.license) {
      out += '\n' + CLI.color.optional('License: ' + src.license)
      if (src.licenseUrl) out += CLI.color.black(' ' + src.licenseUrl)
    }
  }

  CLI.out(out)

  if (started) return

  CLI.out(
    CLI.color.short(
      `Want to try this puzzle? Enter: "puzzles start ${puzzle.id}"`,
    ),
  )
}

const listPuzzles = (CLI) => {
  let size = 1
  const data = []
  for (const name of validLevels) {
    size = Math.max(size, name.length)
    data.push({ name, count: 0, solved: 0, selected: false })
  }

  if (data[CLI.puzzleLevel]) data[CLI.puzzleLevel].selected = true

  for (const puzzle of puzzles) {
    data[puzzle.level].count += 1
    if (CLI.puzzle(puzzle.id).solved > 0) data[puzzle.level].solved += 1
  }

  for (const category of data) {
    let out = category.selected ? CLI.color('* ') : '  '
    out += CLI.color.short(category.name.padEnd(size + 1))
    if (category.solved === 0) {
      out += CLI.color(CLI.plural(category.count, 'puzzle'))
    } else if (category.solved === category.count) {
      if (category.count > 3) out += CLI.color.success('all ')
      out += CLI.color.success(CLI.plural(category.count, 'puzzle') + ' solved')
    } else {
      out += CLI.color.success(`${category.solved} solved`)
      out += CLI.color(` of ${category.count} puzzles`)
    }

    CLI.out(out)
  }
}

const listLevel = (CLI, levelName) => {
  let size = 1
  const level = levels.indexOf(levelName)
  const data = []
  for (const puzzle of puzzles) {
    if (puzzle.level !== level) continue
    size = Math.max(size, puzzle.id.length)
    data.push({
      id: puzzle.id,
      title: puzzle.title,
      solved: CLI.puzzle(puzzle.id).solved > 0,
    })
  }

  for (const puzzle of data) {
    let out = CLI.color.success(puzzle.solved ? '✓' : ' ')
    out += CLI.color.short(' ' + puzzle.id)
    if (puzzle.title) out += CLI.color(' ' + puzzle.title)
    CLI.out(out)
  }
}

const startPuzzle = (CLI, puzzle) => {
  const id = CLI.newPuzzle(puzzle)
  CLI.out(CLI.color(`Started puzzle ${puzzle.id} as game #${id}`))
  showPuzzle(CLI, puzzle.id, true)
}

const startNextPuzzle = (CLI, level) => {
  if (level === undefined) level = CLI.puzzleLevel
  const unsolved = puzzles.filter((p) => !CLI.puzzle(p.id).solved)
  if (unsolved.length === 0) {
    CLI.out(CLI.success('You have already completed all the puzzles.'))
    return CLI.error('Specify a specific puzzle to try one again.')
  }

  const next = unsolved.find((p) => p.level >= level) || unsolved[0]
  startPuzzle(CLI, next)
}

puzzlesCmd.fn = (CLI, action, id) => {
  action = action?.toLowerCase() ?? 'list'
  if (startAliases.some((alias) => alias.startsWith(action))) {
    if (!id) return startNextPuzzle(CLI)

    const puzzle = puzzles.find((p) => p.id === id)
    if (puzzle) {
      return startPuzzle(CLI, puzzle)
    }

    if (validLevels.includes(id)) {
      return startNextPuzzle(CLI, levels.indexOf(id))
    }

    const possibles = puzzles.filter((p) => p.id.includes(id))
    if (possibles.length === 1) {
      return startPuzzle(CLI, possibles[0])
    }

    return CLI.error('Could not find puzzle ' + id)
  }

  if (action) {
    const x = id || action
    if (validLevels.includes(x)) return listLevel(CLI, x)
    if (listAliases.some((alias) => alias.startsWith(x))) {
      return listPuzzles(CLI)
    }

    return showPuzzle(CLI, x)
  }

  listPuzzles(CLI)
}
