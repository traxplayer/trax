/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI import/export commands

import fs from 'node:fs/promises'
import process from 'node:process'

import envPaths from 'env-paths'
import makeDir from 'make-dir'

import { Trax } from '@slugbugblue/trax'
import { puzzles, sources } from '@slugbugblue/trax/puzzles'

const paths = envPaths('trax', { suffix: '' })
const PATHS = {}

// Store a copy of the CLI context object locally
let cli = {}

const fsOuch = (fsError) => {
  if (cli.out) {
    cli.out(fsError, cli.COLORS.fatal)
  } else {
    console.log(fsError)
  }

  throw fsError
}

const expandPath = (name) => {
  // Expand home directory ... anything else?
  if (name.includes('~')) name = name.replace('~', process.env.HOME)
  return name
}

// Return the actual path on disk, creating it if necessary.
// Path is one of cache, config, data, log, temp.
const getPath = async (path) => {
  if (path in PATHS) return PATHS[path]
  if (path in paths) {
    const realPath = await makeDir(paths[path])
    PATHS[path] = realPath
    return realPath
  }

  return false
}

const hexy = (length = 6) => {
  let hex = ''
  while (hex.length < length) {
    hex += '0123456789abcdef'[Math.floor(Math.random() * 16)]
  }

  return hex
}

export const getFile = async (filetype, filename, variable, decode) => {
  const path = await getPath(filetype)
  if (path) {
    try {
      const contents = await fs.readFile(`${path}/${filename}`, 'utf8')
      for (const [key, value] of Object.entries(decode(contents))) {
        variable[key] = value
      }
    } catch (fsError) {
      if (fsError instanceof TypeError || fsError instanceof SyntaxError) {
        const error = `Unable to read ${filetype} file ${path}/${filename}`
        if (cli.error) {
          cli.error(error)
        } else {
          console.log(error)
        }
      } else if (fsError.code !== 'ENOENT') {
        fsOuch(fsError)
      }
    }
  }

  return variable
}

export const saveFile = (filetype, filename, data) => {
  getPath(filetype).then((path) => {
    if (path) {
      const parts = filename.split('.')
      parts.splice(1, 0, hexy())
      const tmpfile = path + '/' + parts.join('.')
      filename = path + '/' + filename
      fs.writeFile(tmpfile, data)
        .then(() => {
          fs.rm(filename, { force: true, maxRetries: 5 })
            .then(() => {
              fs.rename(tmpfile, filename).catch(fsOuch)
            })
            .catch(fsOuch)
        })
        .catch(fsOuch)
    }
  })
}

export const importCmd = {
  name: 'import',
  alt: ['load', 'open'],
  args: '<filename>',
  comp: '<file>',
  desc: 'open a game file',
  help: [
    'Load a ".trx" game file into memory. Once imported, it will be assigned',
    'a game id and automatically selected for future commands.',
  ],
}

export const exportCmd = {
  name: 'export',
  alt: ['save', 'keep', 'share'],
  args: '[#id] [filename]',
  comp: '<id> <file>',
  desc: 'save a game to a file',
  help: [
    'Export the current game to an external ".trx" file for safe-keeping or',
    'to share. Specify a game id to export a different game.',
  ],
}

/** Interpret the rules string of a .trx file.
 * @arg {string} r - rules string in lower case
 * @returns {TraxVariant}
 */
const getRules = (r) =>
  r.includes('loop') ? 'traxloop' : r.includes('8x8') ? 'trax8' : 'trax'

/** Decode a .trx formatted string into a game, preserving players and comments.
 * @arg {any} CLI - the CLI object
 * @arg {string} content - the contents of the .trx file
 * @returns {boolean} - true if a game was created
 */
const interpretFile = (CLI, content) => {
  let rules
  let trax
  let players = []
  const moves = []
  const comments = []
  for (const line of content.split('\n')) {
    if (!line) continue
    const [ln, ...comment] = line.split(/[#;]/)
    if (ln) {
      const lower = ln.toLowerCase()
      if (!rules && lower.includes('trax')) {
        rules = getRules(lower)
      } else if (players.length === 0 && lower.includes(' vs ')) {
        players = ln.split(' ').filter(Boolean)
      } else {
        moves.push(...ln.split(' '))
      }
    }

    if (!trax && rules && (players.length > 0 || moves.length > 0)) {
      trax = new Trax(rules)
    }

    if (moves.length > 0) {
      if (!trax) return false
      if (trax.gameOver) {
        comments.push({
          note: moves.map((c) => c.trim()).join(' '),
          move: trax.move,
        })
      } else {
        trax.playMoves(moves.filter(Boolean))
        moves.splice(0, moves.length)
      }
    }

    if (comment.length > 0) {
      const note = comment.map((c) => c.trim()).join(' ')
      if (!/^@slugbugblue\/trax cli\.js v/.test(note)) {
        comments.push({
          note,
          move: trax?.move || 0,
        })
      }
    }
  }

  if (!trax) return false

  CLI.do('new', rules, ...players)
  CLI.GAMES[String(CLI.GAME.id)].notes = comments
  if (trax.moves.length > 0) {
    CLI.do('play', ...trax.moves)
  } else {
    CLI.save()
  }

  return true
}

importCmd.fn = async (CLI, filename) => {
  cli = CLI
  if (!filename) {
    CLI.error('Missing filename.')
    return CLI.do('help', 'import')
  }

  let content
  try {
    content = await fs.readFile(expandPath(filename), 'utf8')
  } catch (fsError) {
    if (fsError.code === 'ENOENT' && !filename.endsWith('.trx')) {
      try {
        content = await fs.readFile(expandPath(filename + '.trx'), 'utf8')
      } catch {
        content = null
      }
    }

    if (!content) return CLI.error(fsError.toString())
  }

  if (!interpretFile(CLI, content)) {
    CLI.error(filename + ' does not appear to be formatted correctly.')
  }
}

/** Get all the notes for a certain move.
 * @arg {number} move - The move number
 * @arg {GameNotes} notes? - The notes for a game
 * @returns {string} - Notes formatted for this move, or empty string if none
 */
const gameNotes = (move, notes) => {
  notes = notes || []
  const moveNotes = notes.filter((n) => n.move === move)
  if (moveNotes.length === 0) return ''
  return (
    '; ' +
    moveNotes.map((n) => n.note.replace(/\n/g, '\n; ')).join('\n; ') +
    '\n'
  )
}

/** Join two strings together with a character with the appropriate joining
 * character depending on the length of the strings.
 * @arg {string} a - the first string
 * @arg {string} b - the second string
 * @returns {string} - both strings correctly joined
 */
const join = (a, b) =>
  a.length === 0 ? b : a.length + b.length > 78 ? a + '\n' + b : a + ' ' + b

/** Interleave the game moves with any notes, formatted for the export file.
 * @arg {string[]} moves - an array of move notations
 * @arg {GameNotes} notes? - The notes for a game
 * @returns {string} - A string that can be placed into an export file
 */
const interleaveNotation = (moves, notes) => {
  let content = ''
  let moveNumber = 0
  let annotation = ''
  for (const move of moves) {
    const comments = gameNotes(moveNumber, notes)
    if (comments) {
      content += join(annotation, comments)
      annotation = ''
    }

    moveNumber += 1
    const notation = String(moveNumber) + '. ' + move
    if (annotation.length + notation.length < 79) {
      annotation += (annotation.length > 0 ? ' ' : '') + notation
    } else {
      content += '\n' + annotation
      annotation = notation
    }
  }

  const comments = gameNotes(moveNumber, notes)
  if (comments) {
    content += join(annotation, comments)
    annotation = ''
  }

  content += annotation
  if (!content.endsWith('\n')) content += '\n'
  return content
}

exportCmd.fn = (CLI, id, filename) => {
  cli = CLI
  let game = CLI.GAME
  if (id) {
    const newid = id.replace(/^#/, '')
    game = CLI.GAMES[newid]
    if (!game) {
      filename = id
      game = CLI.GAME
    }
  }

  if (!game || !game.id) {
    return CLI.error('No active game. Type "new" to start a game.')
  }

  const trax = new Trax(game.rules, game.moves, 'cli')

  let content = Trax.names[trax.rules].replace(' ', '') + '\n'
  content += game.players?.[0] || 'white'
  content += ' vs '
  content += game.players?.[1] || 'black'
  content += '\n; @slugbugblue/trax cli.js v' + CLI.version + '\n'
  if (game.puzzle) {
    const puzzle = puzzles.find((p) => p.id === game.puzzle)
    if (puzzle) {
      const source = sources[puzzle.src]
      content += '; Puzzle ' + game.puzzle
      if (source) {
        content += source.copyright
          ? ' ©' + source.copyright + ' by '
          : ' provided courtesy of '
        content += source.name
        if (source.url) content += '\n; ' + source.url
        if (source.license) {
          content += '\n; Licensed under ' + source.license
          content += source.licenseUrl ? ' ' + source.licenseUrl : ''
        }
      }

      content += '\n'
    }
  }

  content += interleaveNotation(trax.moves, game.notes)

  if (filename) filename = filename.replace(/[^ a-z\d.~/]/g, '')
  if (filename) {
    const fname = filename.split('/').pop()
    if (!fname) filename += game.rules + game.id
    if (!fname.includes('.')) filename += '.trx'
    CLI.out(CLI.color('Exporting #' + game.id + ' to ') + filename)
    fs.writeFile(expandPath(filename), content).catch(fsOuch)
  } else {
    CLI.out(content)
  }
}

const FOLDERS = {}

export const findFiles = (path) => {
  if (typeof path !== 'string') path = ''
  let folder = '.'
  if (path.includes('/')) {
    folder = path.slice(0, path.lastIndexOf('/'))
  }

  const prefix = folder + '/'

  folder = expandPath(folder)

  if (!FOLDERS[prefix]) {
    FOLDERS[prefix] = []
    fs.readdir(folder, { withFileTypes: true })
      .then((dir) => {
        // This is all happening asynchronously, so we have to call findFiles
        // at least twice before we get any useful information ...
        const entries = []
        for (const entry of dir) {
          if (entry.isDirectory() && !entry.name.startsWith('.')) {
            entries.push(prefix + entry.name + '/')
          } else if (entry.isFile() && entry.name.endsWith('.trx')) {
            entries.push(prefix + entry.name)
          }
        }

        FOLDERS[prefix] = entries
      })
      .catch(() => {})
  }

  return FOLDERS[prefix]
}
