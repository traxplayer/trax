#!/usr/bin/env node
/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI for Trax, with a fallback REPL

import process from 'node:process'
import repl from 'node:repl'

import YAML from 'yaml'

import { Trax } from '@slugbugblue/trax'
import { version } from '@slugbugblue/trax/version'
import * as tty from '@slugbugblue/trax/tty'

// Load all the commands as individual plugins
import { analyzeCmd } from './cmds/analyze.js'
import { deleteCmd } from './cmds/delete.js'
import { helpCmd } from './cmds/help.js'
import {
  importCmd,
  exportCmd,
  getFile,
  saveFile,
  findFiles,
} from './cmds/import-export.js'
import { listCmd } from './cmds/list.js'
import { newCmd } from './cmds/new.js'
import { notesCmd } from './cmds/notes.js'
import { playCmd, tryCmd } from './cmds/play-try.js'
import { puzzlesCmd } from './cmds/puzzles.js'
import { selectCmd } from './cmds/select.js'
import { suggestCmd } from './cmds/suggest.js'
import { undoCmd } from './cmds/undo.js'
import { viewCmd } from './cmds/view.js'

// Because typeof [] and null are both 'object'. is([], 'arr') => true
const is = (variable, type) =>
  Object.prototype.toString
    .call(variable)
    .slice(8, -1)
    .toLowerCase()
    .startsWith(type)

// Sample command:
const versionCmd = {
  name: 'version',
  opts: ['--version', '-v'],
  desc: 'display version number',
  help: 'Print out the current version of this program.',
  fn: (CLI) => CLI.out('Trax CLI v' + version),
}

const quitCmd = {
  name: 'quit',
  alt: 'exit',
  desc: 'press <Ctrl-D> to exit',
  help: 'To exit, press Ctrl-D on a blank line or type "quit".',
  hide: true,
  fn() {
    // I actually have no idea why a broken promise is required here,
    // but I tried a lot of different options, and this is what worked
    return new Promise(() => {
      if (CLI.repl) {
        CLI.repl.close()
      }
    })
  },
}

const plugins = [
  analyzeCmd,
  deleteCmd,
  exportCmd,
  helpCmd,
  importCmd,
  listCmd,
  newCmd,
  notesCmd,
  playCmd,
  puzzlesCmd,
  quitCmd,
  selectCmd,
  suggestCmd,
  tryCmd,
  undoCmd,
  versionCmd,
  viewCmd,
]

const newId = (DATA) => {
  let max = 0
  let min = Number.POSITIVE_INFINITY
  if (DATA.games) {
    for (const id of Object.keys(DATA.games)) {
      max = Math.max(max, Number(id) || 0)
      min = Math.min(min, Number(id) || 0)
    }
  } else {
    DATA.games = {}
  }

  if (max === 0 || min <= 1) return String(max + 1)

  return String(min - 1)
}

/** Colorize text according to simplistic and not very complete patterns.
 * @type Colorer
 */
// @ts-ignore
const color = (text, def) => {
  let out = ''
  let quote = false
  let id = false
  const colors = [def]
  const c = (t) => tty.color(t, colors[colors.length - 1] || CLI.COLORS.default)
  const short = (t) => tty.color(t, CLI.COLORS.short) + c()
  for (const char of text) {
    switch (char) {
      case '"': {
        if (quote) {
          colors.pop()
        } else {
          colors.push(CLI.COLORS.command)
        }

        out += c('')
        quote = !quote
        break
      }

      case '#': {
        id = true
        colors.push(CLI.COLORS.id)
        out += c('#')
        break
      }

      case '|': {
        out += short('|')
        break
      }

      case '[': {
        colors.push(CLI.COLORS.optional)
        out += short('[')
        break
      }

      case ']': {
        colors.pop()
        out += short(']')
        break
      }

      case '<': {
        colors.push(CLI.COLORS.variable)
        out += c('')
        break
      }

      case '>': {
        colors.pop()
        out += c('')
        break
      }

      default: {
        if (id && !/[\da-z]/.test(char)) {
          id = false
          colors.pop()
          out += c(char)
        } else {
          out += char
        }
      }
    }
  }

  return tty.color(out, def || CLI.COLORS.default)
}

// Internal representations of files
const CONFIG = { id: '', puzzleLevel: 0 }
const DATA = { games: {}, puzzles: {} }

// CLI context object for plugins
const CLI = {
  // Quick access to all stored games
  get GAMES() {
    return DATA.games || {}
  },
  // And information about the current game
  get GAME() {
    return (CONFIG.id && DATA.games?.[CONFIG.id]) || {}
  },
  // And the current game ID
  get ID() {
    return CONFIG.id
  },
  // The current puzzle level
  get puzzleLevel() {
    return CONFIG.puzzleLevel
  },

  // Provide access to the current Trax object
  TRAX: new Trax(),

  // Color codes
  COLORS: {
    black: 240,
    command: 33,
    default: 39,
    error: 205,
    fatal: 127,
    help: 74,
    id: 195,
    variable: 123,
    optional: 110,
    short: 153,
    success: 35,
    white: 250,
  },

  // All of the above COLORS will be populated automatically into "color", where
  // they can be called as functions, to apply the given color, eg:
  // CLI.color('hi') and CLI.color.default('hi') are equivalent
  color,

  cmd(command) {
    // Return a command structure object or an empty object
    if (is(command, 'str') && command in commands) {
      return commands[command]
    }

    return {}
  },

  get commands() {
    return Object.keys(commands)
  },

  delete(id) {
    delete DATA.games[id]
    saveData()
  },

  do(cmd, ...args) {
    // Call another plugin
    if (cmd in commands) {
      commands[cmd].fn(CLI, ...args)
    } else {
      throw new SyntaxError(`${cmd} is not a valid command`)
    }
  },

  async doNext(...cmd) {
    // Execute another command line
    await evaluate(cmd.join(' '))
  },

  error(text) {
    // Prints an error to stdout
    CLI.out(CLI.color.error(text))
  },

  // Notation manipulation
  fixNotation(move) {
    if (move.endsWith('b')) return move.slice(0, -1) + '\\'
    if (move.endsWith('s')) return move.slice(0, -1) + '/'
    if (move.endsWith('p')) return move.slice(0, -1) + '+'
    return move
  },

  load(id) {
    if (!id) id = Object.keys(DATA.games || {})[0]
    if (id && DATA.games?.[id] && DATA.games[id].rules) {
      if (id !== String(CONFIG.id)) {
        CONFIG.id = id
        saveConfig()
      }

      const game = CLI.GAME
      if (!game.players) game.players = ['white', 'black']
      CLI.TRAX = new Trax(game.rules, game.moves, 'cli')
    }
  },

  newGame(rules, players, moves) {
    // Creates a new game
    const id = newId(DATA)
    const trax = new Trax(rules, moves, 'cli')
    DATA.games[id] = {
      id,
      rules: trax.rules,
      name: trax.name,
      turn: trax.turn,
      players,
      moves: trax.moves.join(' '),
    }

    CONFIG.id = id
    CLI.TRAX = trax

    saveConfig()
    saveData()

    return id
  },

  newPuzzle(puzzle) {
    // Starts a new puzzle
    const id = newId(DATA)
    const trax = new Trax(puzzle.game, puzzle.notation, 'cli')
    const players = ['challenger', 'puzzlebot']
    const notes = []
    if (puzzle.title) notes.push({ move: 0, note: puzzle.title })
    if (puzzle.desc) notes.push({ move: 0, note: puzzle.desc })
    const player = [0, 'white', 'black'][puzzle.player]
    notes.push(
      { move: 0, note: 'Starting position ' + puzzle.notation },
      { move: 0, note: `${puzzle.id}: ${player} to win by move ${puzzle.max}` },
    )
    DATA.games[id] = {
      id,
      puzzle: puzzle.id,
      rules: trax.rules,
      name: trax.name,
      turn: trax.turn,
      max: puzzle.max,
      players: trax.turn === 1 ? players : players.reverse(),
      moves: trax.moves.join(' '),
      notes,
    }

    if (DATA.puzzles[puzzle.id]) {
      DATA.puzzles[puzzle.id].attempts += 1
    } else {
      DATA.puzzles[puzzle.id] = { attempts: 1, solved: 0 }
    }

    CONFIG.id = id
    CONFIG.puzzleLevel = puzzle.level
    CLI.TRAX = trax

    saveConfig()
    saveData()

    return id
  },

  out(text) {
    // Prints to stdout
    console.info(text + tty.color(''))
  },

  plural(n, noun, nouns) {
    return String(n) + ' ' + (Math.abs(n) === 1 ? noun : nouns || noun + 's')
  },

  puzzle(id) {
    return DATA.puzzles?.[id] || { attempts: 0, solved: 0 }
  },

  puzzleSolved(id) {
    DATA.puzzles = DATA.puzzles || {}
    DATA.puzzles[id] = DATA.puzzles[id] || { attempts: 1, solved: 0 }
    DATA.puzzles[id].solved += 1
  },

  resolveCommand(word) {
    // Determine if an entered word is a valid command.
    // Returns the command if a single match is found, an array if multiple
    // commands match, or undefined if nothing matches.
    if (is(word, 'str') && word.length > 0) {
      return abbreviations[word.toLowerCase()]
    }

    return undefined
  },

  /** Write out the current data to disk. */
  save() {
    saveData()
  },

  setGame(moves) {
    // Updates the moves for the current game
    const game = CLI.GAME
    const trax = new Trax(game.rules, moves, 'cli')
    if (game.over || trax.over) game.over = trax.over
    game.moves = trax.moves.join(' ')
    game.turn = trax.turn
    CLI.TRAX = trax

    saveData()
  },

  updateGameData() {
    const data = DATA.games[CLI.GAME.id]
    const moves = CLI.TRAX.moves.join(' ')
    if (data.moves !== moves) {
      data.moves = moves
      data.turn = CLI.TRAX.turn
      if (CLI.TRAX.over) data.over = true
      saveData()
    }
  },

  // Pass through functions
  bubble: tty.bubble,
  display: tty.display,
  name: tty.name,

  is,
  version,
}

let startRepl = false

const getConfig = async () => {
  await getFile('config', 'trax.yaml', CONFIG, YAML.parse)
  return CONFIG
}

const getData = async () => {
  await getFile('data', 'trax.json', DATA, JSON.parse)
  return DATA
}

const saveConfig = () => {
  saveFile('config', 'trax.yaml', YAML.stringify(CONFIG))
}

const saveData = () => {
  saveFile('data', 'trax.json', JSON.stringify(DATA, null, 2) + '\n')
}

const OPTS = {}

const parseCommandLine = () => {
  let cmd = ''
  const args = process.argv.slice(2)
  let dash = true
  for (const arg of args) {
    if (dash && arg.startsWith('-')) {
      if (arg === '--') dash = false
      if (arg === '-i' || arg === '--interactive') startRepl = true
      if (arg in OPTS) {
        cmd = OPTS[arg] + ' ' + cmd
      }
    } else {
      cmd += arg + ' '
    }
  }

  return cmd.slice(0, -1)
}

// Auto-populate the color functions
for (const [name, value] of Object.entries(CLI.COLORS)) {
  CLI.color[name] = (text) => color(text, value)
}

const forceArray = (item) =>
  is(item, 'arr') ? item : is(item, 'undef') ? [] : [item]

const abbreviations = {}
const commands = {}
const aliases = []
const regexes = []

const addAbbrev = (abbr, full) => {
  for (let abbrev of abbr) {
    while (abbrev.length > 0) {
      const abbrSet = abbreviations[abbrev] || new Set()
      abbrSet.add(full)
      abbreviations[abbrev] = abbrSet
      abbrev = abbrev.slice(0, -1)
    }
  }
}

const makeComp = (comp) => {
  if (is(comp, 'arr') || is(comp, 'func')) return comp
  if (is(comp, 'str')) return comp.split(' ')
  return []
}

const processPlugins = () => {
  // Process all the plugin command entries
  for (const cmd of plugins) {
    if (cmd.opts) {
      for (const opt of forceArray(cmd.opts)) {
        OPTS[opt] = cmd.name
      }
    }

    if (cmd.rx) {
      for (const rx of forceArray(cmd.rx)) {
        regexes.push({ rx, name: cmd.name })
      }
    }

    const entry = {
      alt: forceArray(cmd.alt),
      args: cmd.args || '',
      comp: makeComp(cmd.comp),
      desc: cmd.desc,
      help: forceArray(cmd.help),
      hide: cmd.hide,
      fn: cmd.fn,
    }
    addAbbrev([cmd.name, ...entry.alt], cmd.name)
    commands[cmd.name] = entry

    aliases.push(cmd.name)
    for (const alias of entry.alt) {
      aliases.push(alias)
    }
  }

  for (const [abbr, value] of Object.entries(abbreviations)) {
    const newValue = [...value]
    abbreviations[abbr] = value.size > 1 ? newValue.sort() : newValue[0]
  }
}

const expand = (completions, match) => {
  completions = [...completions] // Do not modify in-place
  if (completions.includes('<cmd>')) {
    // All commands
    completions[completions.indexOf('<cmd>')] = aliases.join('|')
  }

  if (completions.includes('<id>')) {
    // Game numbers
    const ids = []
    for (const game of Object.values(DATA.games || {})) {
      if (game.id) ids.push('#' + game.id)
    }

    completions[completions.indexOf('<id>')] = ids.join('|')
  }

  if (completions.includes('<move>')) {
    // Move numbers
    const moves = []
    for (let move = 1; move <= CLI.TRAX.moves.length; move++) {
      moves.push(String(move))
    }

    completions[completions.indexOf('<move>')] = moves.join('|')
  }

  if (completions.includes('<play>')) {
    // Moves
    const moves = CLI.TRAX.possibleMoves()
    completions[completions.indexOf('<play>')] = moves.join('|')
  }

  if (completions.includes('<file>')) {
    // Files ... a bit tricky since we don't want to async in the middle of
    // a completion, but we do want to list all the .trx files in the folder
    // we are looking for ... so we cheat and initiate a search on the first
    // call, and maybe have completions by the second or third call ...
    completions[completions.indexOf('<file>')] = findFiles(match).join('|')
  }

  return completions
}

const completer = (line) => {
  const words = line.split(/\s+/).filter(Boolean)

  let match = words.slice(-1)[0] || ''
  const previous = new Set(words.slice(1, -1).map((s) => s.toLowerCase()))
  if (line.endsWith(' ')) {
    previous.add(match.toLowerCase())
    match = ''
  }

  let completions = []
  const cmd = abbreviations[String(words[0]).toLowerCase()]

  if (is(cmd, 'str') && (line.endsWith(' ') || words.length > 1)) {
    completions = commands[cmd].comp
    if (is(completions, 'func')) {
      completions = completions(CLI, words.slice(1), aliases)
    }

    completions = expand(completions, match)
    if (words.length === 1 || (words.length === 2 && !line.endsWith(' '))) {
      completions.push('help', '?')
    }
  } else {
    completions = aliases
  }

  for (let i = 0; i < completions.length; i++) {
    const word = completions[i]
    if (word.includes('|')) {
      const comps = word.split('|')
      if (comps.some((c) => previous.has(c.toLowerCase()))) {
        completions[i] = ''
      } else {
        completions[i] = comps
      }
    }
  }

  completions = completions
    .flat()
    .filter((c) => c && !previous.has(c.toLowerCase()))

  if (match && completions.length > 0) {
    completions = completions.filter((c) => {
      return c.toLowerCase().startsWith(match.toLowerCase())
    })
  }

  if (!match && completions.length === 1) completions.push(' ')

  completions = completions.map((c) => c + ' ').sort()

  return [completions, match]
}

const ambiguousSuggestions = (cmd) => {
  const maybe = new Set()
  for (const alias of cmd) {
    if (alias.startsWith(CLI.CMD)) maybe.add(alias)

    let alts = commands[alias].alt
    alts = is(alts, 'arr') ? alts : [alts || '']

    for (const alt of alts) {
      if (alt.startsWith(CLI.CMD)) maybe.add(alt)
    }
  }

  return maybe
}

const evaluate = async (command) => {
  const [CMD, ...words] = command.split(/\s+/).filter(Boolean)
  const cmdLower = CMD?.toLowerCase() || ''
  CLI.CMD = cmdLower
  let cmd = abbreviations[cmdLower]

  // If we are not in a REPL, let's remove hidden commands
  if (!CLI.repl && is(cmd, 'arr')) {
    cmd = cmd.filter((c) => !commands[c].hide)
    if (cmd.length === 1) cmd = cmd[0]
  }

  if (is(cmd, 'str')) {
    if (words.length > 0 && abbreviations[words[0].toLowerCase()] === 'help') {
      commands.help.fn(CLI, cmd)
    } else {
      await commands[cmd].fn(CLI, ...words)
    }
  } else if (is(cmd, 'arr')) {
    CLI.error('Ambiguous command. Perhaps you meant one of the following:')
    const maybe = ambiguousSuggestions(cmd)

    if (maybe.size > 0) {
      for (const alias of [...maybe].sort()) {
        tty.outty(CLI.color.command('  ' + alias))
      }
    }
  } else if (CMD) {
    let rxCmd
    for (const regex of regexes) {
      if (regex.rx.test(CMD)) rxCmd = regex.name
    }

    if (rxCmd) {
      await commands[rxCmd].fn(CLI, CMD, ...words)
    } else {
      CLI.error('Unknown command. Type "help" for a list of valid commands.')
    }
  }
}

const main = async () => {
  processPlugins()
  const command = parseCommandLine()
  if (is(command, 'str')) {
    await Promise.all([getConfig(), getData()])
    CLI.load(CONFIG.id)
    if (command) await evaluate(command)
    if (startRepl || !command) {
      if (!command) {
        tty.outty(color('Welcome to the Trax CLI v' + version + '.'))
        tty.outty(color('Type "help" for more information.'))
      }

      CLI.repl = repl.start({
        prompt: 'trax❯ ',
        async eval(cmd, ctx, fn, cb) {
          try {
            await evaluate(cmd)
          } catch (error) {
            tty.outty(error, CLI.COLORS.fatal)
            throw error
          }

          cb(null, undefined)
        },
        ignoreUndefined: true,
        completer,
        preview: true,
      })
    }
  }
}

await main()
