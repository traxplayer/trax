/** Type information.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** Color is a single character to represent white or black. */
type Color = 'w' | 'b'

/** Colorize is a fancy function for colorizing text. */
type Colorize = (text: string, def?: string | number) => string

/** Colorer is a little too fancy. Hence the gnarly typescript. */
type Colorer = {
  (text: string, def?: string | number): string
  black: Colorize
  command: Colorize
  default: Colorize
  error: Colorize
  fatal: Colorize
  help: Colorize
  id: Colorize
  variable: Colorize
  optional: Colorize
  short: Colorize
  white: Colorize
}

/** An object representing a space on the perimeter of a Trax position.
 * x,y: the coordinates of the space
 * c: the color present beside the space (w, b, l: both w and b, r: none)
 * t: the type of possible moves: n-normal, x-none, c-cave, C-restricted cave
 * b,w: if present, the number of the line to match with its other end
 * pb, pw: if present, can pair with the next line in one turn
 * xb, xw: the label of this space for each color
 */
type EdgeLocation = {
  x: number
  y: number
  c: string
  t: string
  b?: number
  w?: number
  pb?: boolean
  pw?: boolean
  zb?: string
  zw?: string
  idx?: number
}

type EdgeObject = {
  b: string
  w: string
  edge: RawEdge
}

/** Object representing a concrete threat found in a position. */
type FoundThreat = {
  threat: string
  match: string
  at: number
  value: number
  level: number
}

/** Threats found for each color that cannot actually be activated. */
type FaultyThreats = {
  b: FoundThreat[]
  w: FoundThreat[]
}

/** An object with keys representing each level, with arrays for each threat of
 * that level. */
type ColorThreats = Record<string, FoundThreat[]>

/** All the threats found for both the white and black player. */
type FoundThreatsCollection = { b: ColorThreats; w: ColorThreats }

/** A note with a move number */
type GameNote = {
  move: number
  note: string
}

/** Notes saved in the game object in the CLI. */
type GameNotes = GameNote[]

/** A point-like object has numerical x and y properties. */
type PointLike = {
  x: number
  y: number
}

/** An object used to track an analysis on a position. */
type PositionScore = {
  move: string
  score: number
  analysis?: Analysis
}

/** A puzzle. */
type Puzzle = {
  id: string
  src: string
  game: TraxVariant
  notation: string
  icon: string
  level: number
  max: number
  player: number
  title?: string
  desc?: string
  hint?: string
  hints?: string[]
}

type PuzzleSource = {
  name: string
  url?: string
  copyright?: string
  license?: string
  licenseUrl?: string
}

/** Array of objects representing the spaces surrounding the perimeter of a
 * Trax position. */
type RawEdge = EdgeLocation[]

/** Treat the save state as an opaque object,
 * produced by save() and fed into restore().
 */
type SaveState = {
  id: string
  move: number
  turn: number
  over: boolean
  left: number
  right: number
  top: number
  bottom: number
  notation: string
  tiles: string
  path: string
  invalid: boolean
}

/** Representations of the different ways a tile can curve.
 * Matches the symbols used in Trax notation.
 */
type Slash = '/' | '\\' | '+'

/** Threat definition. */
type Threat = {
  depth: number
  pattern: string
  rx: RegExp
  value: number
}

/** A single tile on the board. */
type Tile = {
  id: TileId
  loc: Point
  type: TileType
  move: number
  seq: number
}

/** When a tile is dropped, this object represents the results. */
type TileDrop = {
  dropped: Tile[]
  notation: string
  valid: boolean
}

/** A TileId is just a string. */
type TileId = string

/** Invalid tiles are represented by 'x'. */
type TileType = ValidTiles | 'x'

/** All of the variants supported by the engine. */
type TraxVariant = 'trax' | 'traxloop' | 'trax8'

/** Tile types are represented by one of the following single characters. */
type ValidTiles = 'a' | 'b' | 'c' | 'd' | 'e' | 'f'
