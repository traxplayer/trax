/* Copyright 2021-2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Ever wanted to draw Trax boards on the console? I have.

import process from 'node:process'

import { Trax } from '@slugbugblue/trax'

const esc = '\u001B[' // Start of console color escape code
const off = esc + 'm' // Color reset code
const ENV = process.env
const NERD = ENV.NERDFONT || ENV.NERDFONTS
const POWER = ENV.POWERLINE || ENV.P9K_TTY || ENV.P9K_SSH
const leftBubble = NERD ? '' : POWER ? '' : '▐'
const rightBubble = NERD ? '' : POWER ? '' : '▌'

/** Colorize a string.
 * @arg {string} contents
 * @arg {string|number} [fgcolor]
 * @arg {string|number} [bgcolor]
 */
export const color = (contents = '', fgcolor = '', bgcolor = '') => {
  const c = []
  if (fgcolor) c.push('38;5;' + fgcolor)
  if (bgcolor) c.push('48;5;' + bgcolor)
  return esc + c.join(';') + 'm' + contents
}

// Display a message with optional color
export const outty = (message, fgcolor, bgcolor) => {
  console.log(color(message, fgcolor, bgcolor) + off)
}

// A little magic to display the game in progress
const colorSets = {
  // Colors for terminal sequence: <Esc>[38;5;<color_num>m
  // b, B, w, W: color of line for each player, capital for a winning line
  // e: empty (ie, board) color, t: tile color
  trax: { b: 238, B: 57, e: 19, t: 74, w: 255, W: 184 },
  trax8: { b: 236, B: 21, e: 58, t: 137, w: 254, W: 117 },
  traxloop: { b: 235, B: 20, e: 17, t: 61, w: 253, W: 222 },
  tentative: { b: 235, B: 18, e: 19, t: 244, w: 252, W: 217 },
}

let set = colorSets.trax // Which color set should we draw with?

export const name = (rules) => {
  if (Trax.variants.has(rules)) {
    return color(Trax.names[rules], colorSets[rules].t)
  }

  return rules
}

export const bubble = (clr, text) => {
  const colors = {
    w: { fg: 236, bg: 153 },
    b: { fg: 250, bg: 18 },
    wd: { fg: 236, bg: 248 },
    bd: { fg: 248, bg: 236 },
    wh: { fg: 63, bg: 123 },
    bh: { fg: 220, bg: 20 },
  }

  const c = colors[clr]
  return (
    color(leftBubble, c.bg) +
    color(text, c.fg, c.bg) +
    off +
    color(rightBubble, c.bg)
  )
}

const ln = 'm\u2584' // Lower half block -- with the leading m to terminate ansi color escape
const up = 'm\u2588' // Full block
const lt = 'm\u2599' // Lower half and left half block
const rt = 'm\u259F' // Lower half and right half block
const fg = (n) => '38;5;' + String(set[n]) // Foreground color_num
const bg = (n) => '48;5;' + String(set[n]) // Background color_num
const sp = () => esc + bg('e') + 'm ' // Lower space
// tiles are displayed on two lines; the top half of the first line is empty
// and the second half is composed of three characters, up color surrounded by tile color
const uptty = (clr) => {
  const left = esc + fg('t') + ';' + bg('e') + ln
  const center = esc + fg(clr) + ln
  const right = esc + fg('t') + ln + ' '
  return left + center + right
}

// The top half of the second line has both horizontal lines with a center transition
// while the bottom half is the tile background color surrounding the down color
const horiztty = (clr) => esc + fg('t') + ';' + bg(clr) + ln
const dntty = (clr, oppclr, char) =>
  esc + fg(clr) + (char === up ? '' : ';' + bg(oppclr)) + char
// List of icon_char => tile line colors for easy reference in order up, right, down, left
const tiles = {
  a: 'bbww',
  b: 'bwbw',
  c: 'bwwb',
  d: 'wbbw',
  e: 'wbwb',
  f: 'wwbb',
}

// Return the ANSI codes to display a 3x1.5 block tile, one line at a time
const tileTty = (tile, top) => {
  // Top: true for first line, false for second
  // quick access to tile color and its opposite in upper case
  const colors = { b: 'b', w: 'w', B: 'w', W: 'b' }
  const save = set
  const tentative = tile !== tile.toLowerCase()
  if (tentative) set = colorSets.tentative
  tile = tile.toLowerCase()
  if (!(tile in tiles)) {
    // Winning paths are represented by adding 6 to the tile char code
    tile = String.fromCodePoint(tile.codePointAt(0) - 6)
    if (tile in tiles) {
      colors.w = 'W'
      colors.B = 'W'
    } else {
      // And another six for a black win
      tile = String.fromCodePoint(tile.codePointAt(0) - 6)
      colors.b = 'B'
      colors.W = 'B'
    }
  }

  let value = ''
  if (tile in tiles) {
    tile = tiles[tile]
    if (top) {
      value = uptty(colors[tile[0]])
    } else {
      const centerChar =
        tile[1] === tile[2] ? rt : tile[2] === tile[3] ? lt : up
      const centertty = dntty(
        colors[tile[2]],
        colors[tile[2].toUpperCase()],
        centerChar,
      )
      value =
        horiztty(colors[tile[3]]) + centertty + horiztty(colors[tile[1]]) + sp()
    }
  }

  set = save
  return value || (top ? '??? ' : "''' ")
}

const mergeTentative = () => {
  colorSets.tentative.e = set.e // Copy empty color to tentative set
  colorSets.tentative.W = set.W
  colorSets.tentative.B = set.B
}

const printIcon = (icon, setName) => {
  set = colorSets[setName] || colorSets.trax
  mergeTentative()
  const on = esc + '38;5;' + String(set.t) + ';48;5;' + String(set.e) + 'm'
  let o1 = '   '
  let o2 = '1  '
  let row = 1
  let width = 1
  const info = []
  let lineWidth = 0
  while (icon.length > 0) {
    const c = icon[0]
    icon = icon.slice(1)
    if (c === ':') {
      info.push({ o: o1, w: lineWidth }, { o: o2, w: lineWidth })
      row++
      width = Math.max(width, lineWidth)
      lineWidth = 0
      o1 = '   '
      o2 = String(row).padEnd(3)
    } else if (c >= '0' && c <= '9') {
      const empty = Number(c) + 1
      o1 += ' '.repeat(empty * 4)
      o2 += ' '.repeat(empty * 4)
      lineWidth += empty
    } else {
      o1 += tileTty(c, 'top')
      o2 += tileTty(c)
      lineWidth++
    }
  }

  if (o1.length > 0) {
    info.push({ o: o1, w: lineWidth }, { o: o2, w: lineWidth })
    width = Math.max(width, lineWidth)
  }

  const columns = process.stdout.columns || 0
  const rows = process.stdout.rows || 0
  if (width * 4 < columns - 4 && info.length < rows - 2) {
    let colOut = '   '
    for (let col = 1; col <= width; col++) {
      colOut += ' ' + Trax.encodeCol(col).padEnd(3)
    }

    console.info(on + colOut.padEnd((width + 1) * 4) + off)
    for (const line of info) {
      console.info(on + line.o + ''.padEnd((width - line.w) * 4) + ' ' + off)
    }

    console.info(on + ''.padEnd((width + 1) * 4) + off)
  } else {
    console.warn('Console is too small to display this game')
  }
}

/** Flexible way to get a Trax game object.
 * @arg {Trax|TraxVariant} [game] - existing Trax instance or a string indicating the variant name
 * @arg {string|string[]} [moves] - optional list or string of moves to be made on the game
 * @returns {Trax} a Trax instance
 */
export const traxGame = (game = 'trax', moves = '') => {
  let trax
  if (game instanceof Trax) {
    trax = game
    if (moves) {
      if (typeof moves === 'string') moves = moves.split(' ')
      for (const move of moves) trax.dropTile(move)
    }
  } else {
    trax = new Trax(game, moves, 'cli')
  }

  return trax
}

const printNotation = (trax, tentative, show = Number.POSITIVE_INFINITY) => {
  let b = false
  let n = 1
  let out = ''
  let length = 0
  const t = tentative ? trax.moves.length - 1 : show
  for (const move of trax.moves) {
    const c = (b ? 'b' : 'w') + (t < n ? 'd' : show === n ? 'h' : '')
    const notation = String(n) + ' ' + move
    if (length + notation.length + 3 > process.stdout.columns) {
      out += '\n'
      length = 0
    }

    out += bubble(c, notation) + ' '
    length += notation.length + 3
    b = !b
    n++
  }

  outty(out)
}

export const display = (
  game,
  players,
  tentative = '',
  showMove = Number.POSITIVE_INFINITY,
) => {
  const trax = traxGame(game)
  const saved = trax.save()
  const colors = ['white', 'black']
  if (!Array.isArray(players)) players = colors
  let { icon } = trax
  if (tentative) {
    const drops = trax.dropTile(tentative)
    if (drops.valid || drops.dropped.length > 0) {
      icon = trax.dropsIcon(drops)
    } else {
      tentative = ''
    }
  } else if (showMove <= trax.moves.length) {
    icon = trax.dropsIcon({
      dropped: Object.values(trax.tiles).filter((t) => t.move > showMove),
    })
  }

  printNotation(trax, tentative, showMove)
  printIcon(icon, trax.rules)
  const nameColor = { 1: 'w', 2: 'b' }[trax.turn]
  if (tentative) {
    const otherColor = { w: 'bd', b: 'wd' }[nameColor] || 'wd'
    outty('---' + bubble(otherColor, 'tentative position') + off + '---')
  }

  const name = players[trax.turn - 1] || colors[trax.turn - 1]
  if (trax.over) {
    if (trax.turn) {
      outty(bubble(nameColor + 'h', name) + off + ' wins')
    } else {
      outty('game end' + (tentative ? 's' : 'ed') + ' in a draw')
    }
  } else if (trax.turn) {
    outty(
      (tentative ? 'leaving ' : '') +
        bubble(nameColor, name) +
        off +
        ' to play',
    )
  }

  if (tentative) trax.restore(saved)
}
