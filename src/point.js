/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** Because javascript doesn't have a "0 < 3 < 7" syntax.
 * @arg {number} end1 - A number on one end or the other
 * @arg {number} mid - The middle number
 * @arg {number} end2 - The other end
 * @returns {boolean} - If the number is in the middle
 */
const between = (end1, mid, end2) => {
  const low = Math.min(end1, end2)
  const high = Math.max(end1, end2)
  return low <= mid && mid <= high
}

/** Are you tired of doing x,y movements in multiple statements?
 *
 * I know I am.
 *
 * Point: an immutable two-dimensional location (x,y) with
 * functions for moving around quickly to neighboring points.
 */
export class Point {
  #x = 0
  #y = 0

  static dirs = ['up', 'down', 'left', 'right']

  /** You can call this with new Point(point) or new Point({x, y}) or new Point(x, y)
   * @arg {number | PointLike} x - the x value or a point
   * @arg {number} [y] - the y value
   */
  constructor(x, y) {
    if (
      typeof x === 'object' &&
      typeof x.x === 'number' &&
      typeof x.y === 'number'
    ) {
      this.#x = x.x
      this.#y = x.y
    } else {
      if (typeof x !== 'number' || Number.isNaN(x)) {
        throw new SyntaxError('x must be a number or a point-like object')
      }

      if (typeof y !== 'number' || Number.isNaN(x)) {
        throw new SyntaxError('y must be a number')
      }

      this.#x = x
      this.#y = y
    }
  }

  /** The horizontal value of the point. */
  get x() {
    return this.#x
  }

  /** Throws an error. Points are immutable. */
  set x(_) {
    throw new ReferenceError('Point is immutable')
  }

  /** The vertical value of the point. */
  get y() {
    return this.#y
  }

  /** Throws an error. Points are immutable. */
  set y(_) {
    throw new ReferenceError('Point is immutable')
  }

  /** The point immediately above this point. */
  get up() {
    return new Point(this.#x, this.#y - 1)
  }

  /** The point immediately below this point. */
  get down() {
    return new Point(this.#x, this.#y + 1)
  }

  /** The point to the immediate left of this point. */
  get left() {
    return new Point(this.#x - 1, this.#y)
  }

  /** The point to the immediate right of this point. */
  get right() {
    return new Point(this.#x + 1, this.#y)
  }

  /** A list of the four points that surround this point: up, down, left, and right. */
  get around() {
    return [this.up, this.down, this.left, this.right]
  }

  /** Move in a specific direction by passing in a direction string.
   * @arg {string} direction - any string that begins with u, d, l, or r
   * @returns {Point}
   */
  dir(direction) {
    const error = 'direction must be one of: up, down, left, right, u, d, l, r'
    if (typeof direction !== 'string') throw new SyntaxError(error)
    direction = direction.toLowerCase()
    if (direction[0] === 'u') return this.up
    if (direction[0] === 'd') return this.down
    if (direction[0] === 'l') return this.left
    if (direction[0] === 'r') return this.right
    throw new SyntaxError(error)
  }

  /** @arg {PointLike} from - the point to measure x against. */
  distX(from) {
    return Math.abs(this.#x - from.x)
  }

  /** @arg {PointLike} from - the point to measure y against. */
  distY(from) {
    return Math.abs(this.#y - from.y)
  }

  /** @arg {PointLike} from - the point to measure against. */
  distance(from) {
    return Math.hypot(this.distX(from), this.distY(from))
  }

  /** @arg {PointLike} that - the point to test equality against. */
  eq(that) {
    return this.#x === that.x && this.#y === that.y
  }

  /** Bounding box corners can be passed in any order.
   * @arg {PointLike} a - one corner of the bounding box
   * @arg {PointLike} b - the opposite corner
   * @returns {boolean} - true if the point is on or in the box
   */
  in(a, b) {
    // Allow for the bounding points to be passed in any order
    // as long as they are opposite corners:
    return between(a.x, this.x, b.x) && between(a.y, this.y, b.y)
  }

  // Play nicely with the rest of the world

  toString() {
    return 'Point(' + this.#x + ',' + this.#y + ')'
  }

  /** @returns {PointLike} */
  toJSON() {
    return { x: this.#x, y: this.#y }
  }

  [Symbol.for('nodejs.util.inspect.custom')](_, options) {
    const [y, s, n] = [options.stylize, 'special', 'number']
    return `${y('Point', s)}(${y(this.#x, n)},${y(this.#y, n)})`
  }
}
