/* Copyright 2019-2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Trax internals. Woot.

// Tile type names are determined by listing the line color at each edge,
// starting from the top and going clockwise, and then sorted alphabetically
// and given a single letter name, so 'bbww' becomes 'a', which gives us six
// different tile names: a-f

import { Point } from '@slugbugblue/trax/point'

// Type definitions
// Fun trax helper constants

const zero = new Point(0, 0)
const moveNumberRegex = /^(\d+)[.):]?$/
const notationRegex = /^([@a-z]+)(\d+)([/\\+])$/i

/** @type ValidTiles[] */
const tileTypes = ['a', 'b', 'c', 'd', 'e', 'f']

/** Tiles are represented by the letters a-f.
 *     a         b        c        d        e        f
 *  +--#--+   +--#--+  +--#--+  +--o--+  +--o--+  +--o--+
 *  |   # |   |  #  |  | #   |  | o   |  |  o  |  |   o |
 *  oo   ##   ooo#ooo  ##   oo  oo   ##  #######  ##   oo
 *  | o   |   |  #  |  |   o |  |   # |  |  o  |  | #   |
 *  +--o--+   +--#--+  +--o--+  +--#--+  +--o--+  +--#--+
 *
 * Slashes are represented by / \ or +.
 * @readonly
 * @type {Record<ValidTiles, Slash>}
 */
const slash = { a: '\\', b: '+', c: '/', d: '/', e: '+', f: '\\' }

/** Symmetry helper to convert from one tile to another.
 * @readonly
 * @type {Record<string, Record<ValidTiles, ValidTiles>>}
 */
const symmetry = {
  // Convert tile to different views
  flip: { a: 'd', b: 'b', c: 'f', d: 'a', e: 'e', f: 'c' },
  mirror: { a: 'c', b: 'b', c: 'a', d: 'f', e: 'e', f: 'd' },
  swap: { a: 'f', b: 'e', c: 'd', d: 'c', e: 'b', f: 'a' },
  counterclock: { a: 'c', b: 'e', c: 'f', d: 'a', e: 'b', f: 'd' },
}

// Fun trax helper functions

/** Apply certain combinations of symmetry.
 * @arg {TileType} tile - the tile to apply symmetry to
 * @arg {boolean} [rightToLeft] - mirror the tile
 * @arg {boolean} [bottomToTop] - flip the tile
 * @arg {boolean} [rotate] - rotate the tile counterclockwise
 * @returns {TileType}
 */
const applySymmetry = (tile, rightToLeft, bottomToTop, rotate) => {
  if (rightToLeft) tile = symmetry.mirror[tile]
  if (bottomToTop) tile = symmetry.flip[tile]
  if (rotate) tile = symmetry.counterclock[tile]
  return tile
}

// Unexpected note: str.includes(t) is true if t is '' but false if t is undefined
/** The color at the top of the tile.
 * @arg {string} t - the tile to check
 * @returns {Color|false} the color or false if t is not a valid tile
 */
const upColor = (t) =>
  'abc'.includes(t) ? 'b' : 'def'.includes(t) ? 'w' : false

/** The color at the bottom of the tile.
 * @arg {string} t - the tile to check
 * @returns {Color|false} the color or false if t is not a valid tile
 */
const downColor = (t) =>
  'bdf'.includes(t) ? 'b' : 'ace'.includes(t) ? 'w' : false

/** The color at the right edge of the tile.
 * @arg {string} t - the tile to check
 * @returns {Color|false} the color or false if t is not a valid tile
 */
const rightColor = (t) =>
  'ade'.includes(t) ? 'b' : 'bcf'.includes(t) ? 'w' : false

/** The color at the left edge of the tile.
 * @arg {string} t - the tile to check
 * @returns {Color|false} the color or false if t is not a valid tile
 */
const leftColor = (t) =>
  'cef'.includes(t) ? 'b' : 'abd'.includes(t) ? 'w' : false

/** Add an empty space to the position encoding string.
 * @arg {string} code - The position encoding string
 * @returns {string}
 */
const addBlank = (code) => {
  // Encode missing tiles as digits 0-9, for 1-10 missing
  const lastNumber = code.codePointAt(code.length - 1) // This is NaN if code is empty, which is ok
  if (lastNumber > 47 && lastNumber < 57) {
    return code.slice(0, -1) + String.fromCodePoint(lastNumber + 1)
  }

  return code + '0'
}

// Ugh, this is a dirty little function to get
/** The length of an encoded position row.
 * @arg {string} row - the encoded row
 * @returns {number} - the actual length of the row
 */
const codeRowLength = (row) =>
  row.replace(/\d/g, (n) => '.'.repeat(Number(n) + 1)).length

// This is where the magic happens
/** A digital representation of a Trax game. */
export class Trax {
  // Static class properties

  /** @readonly @type {Record<TraxVariant, string>} */
  static names = {
    trax: 'Trax',
    traxloop: 'Loop Trax',
    trax8: '8x8 Trax',
  }

  /** @readonly */
  static variants = new Set(Object.keys(this.names)) // Known variants

  // Static class methods

  /** Create an x,y Point object with special functions.
   * @arg {number|PointLike} x - either the x value, or an object with x,y keys
   * @arg {number} [y] - if x is a number, y must be provided as well
   * @returns {Point} a new Point object
   */
  static point = (x, y) => new Point(x, y)

  /** Given a player number, get the color.
   * @arg {number} playerNumber - the player number, 1 or 2
   * @returns {Color} the color of that player, w or b
   */
  static colorOf = (playerNumber) => ({ 1: 'w', 2: 'b' }[playerNumber])

  /** Given a color, get the player number.
   * @arg {string} color - the color, w or b
   * @returns {number} the player number, 1 or 2
   */
  static playerNumber = (color) => ({ b: 2, w: 1 }[color])

  /** Given a color, get the other color.
   * @arg {string} color - the color, w or b
   * @returns {Color} the other color, b or w
   */
  static other = (color) => ({ b: 'w', w: 'b' }[color])

  /** Encode a numeric column number into the Trax notation column letter.
   * @arg {number} col - the colum number, with 0 just to the left of the tiles
   * @returns {string} the encoded column letter
   */
  static encodeCol = (col) => {
    // For notation purposes: 0 -> @, 6 -> F, 28 -> AB
    let n = ''
    while (col > 0) {
      n = String.fromCodePoint(65 + ((col - 1) % 26)) + n
      col = Math.floor((col - 1) / 26)
    }

    return n || '@'
  }

  /** Decode a Trax notation column letter back to a number.
   * @arg {string} col - the Trax column letter
   * @returns {number} the column number
   */
  static decodeCol = (col) => {
    // Turn letters back into numbers
    col = col.toUpperCase()
    let n = 0
    while (col.length > 0) {
      n += (col.codePointAt(0) - 64) * 26 ** (col.length - 1)
      col = col.slice(1)
    }

    return n
  }

  // Trax class constructor
  /** Create a new Trax game
   * @arg {TraxVariant} [rules='trax'] - the variant to play
   * @arg {string|string[]} [moves=[]] - the initial moves to pre-play
   * @arg {string} [id='trax'] - an id used to differentiate tiles from multiple games
   */
  constructor(rules = 'trax', moves = [], id = 'trax') {
    if (!Trax.variants.has(rules)) rules = 'trax'
    this.id = String(id)
    this.rules = rules
    this.move = 0
    this.turn = 1
    this.over = false
    this.left = 1
    this.right = 0
    this.top = 1
    this.bottom = 0
    this.notation = ''
    /** @type {Record<TileId, Tile>} */
    this.tiles = {}
    /** @type TileId[] */
    this.path = []
    this.invalid = false
    // Moves might be a notation string
    if (moves) {
      this.playMoves(moves)
    }
  }

  /** Save the current game data to a variable.
   * @returns {SaveState} an opaque save state object
   * @see restore for restoring the state
   */
  save() {
    // In order to evaluate potential moves, we have to modify the state
    return {
      id: this.id,
      move: this.move,
      turn: this.turn,
      over: this.over,
      left: this.left,
      right: this.right,
      top: this.top,
      bottom: this.bottom,
      notation: this.notation,
      tiles: JSON.stringify(this.tiles),
      path: JSON.stringify(this.path),
      invalid: this.invalid,
    }
  }

  /** Restore a previously saved position.
   * @arg {SaveState} saved - the previously saved state
   * @see save for saving the state
   * @returns {void}
   */
  restore(saved) {
    // So we need to be able to restore it when we are done
    this.id = saved.id
    this.move = saved.move
    this.turn = saved.turn
    this.over = saved.over
    this.left = saved.left
    this.right = saved.right
    this.top = saved.top
    this.bottom = saved.bottom
    this.notation = saved.notation
    this.tiles = JSON.parse(saved.tiles)
    this.path = JSON.parse(saved.path)
    this.invalid = saved.invalid
    // Reconstitute the locations
    for (const tile of Object.values(this.tiles)) {
      tile.loc = new Point(tile.loc)
    }
  }

  /** The name of this variant. */
  get name() {
    return Trax.names[this.rules]
  }

  /** The number of tiles currently in play. */
  get count() {
    return Object.keys(this.tiles).length
  }

  /** The color of the current player, w or b. */
  get color() {
    return Trax.colorOf(this.turn)
  }

  /** True if the game is over. */
  get gameOver() {
    return this.over
  }

  /** 1 or 2 for a win, 0 for a tie, false if the game is still in progress. */
  get winner() {
    return this.over ? this.turn : false
  }

  /** Provides the tile ID for the tile at the given location.
   * @arg {PointLike} loc
   * returns {TileId}
   */
  tileId(loc) {
    return this.id + '-' + String(loc.x) + 'x' + String(loc.y)
  }

  /** Add a tile to the board. Note that no validity checking is done here,
   * except that invalid tiles are not actually placed on the board, so this
   * should only be called externally.
   * @arg {TileType} type
   * @arg {Point} loc
   * @returns {Tile} the tile placed on the board.
   */
  addTile(type, loc) {
    const id = this.tileId(loc)
    const tile = { type, loc, id, move: this.move, seq: this.count }
    if (type === 'x') {
      this.invalid = true // Invalid tile is being played
    } else {
      this.tiles[id] = tile
      this.left = Math.min(this.left, loc.x)
      this.right = Math.max(this.right, loc.x)
      this.top = Math.min(this.top, loc.y)
      this.bottom = Math.max(this.bottom, loc.y)
    }

    return tile
  }

  /** The type of tile at the given location.
   * @arg {PointLike} loc - the location the tile is in
   * @returns {TileType|undefined} the type of tile, or undefined if no tile
   */
  tileAt(loc) {
    return (this.tiles[this.tileId(loc)] || {}).type
  }

  /** Is this tile valid?
   * @type {(type: string, loc: Point) => type is TileType}
   */
  validTile(type, loc) {
    if (this.count === 0 && loc.x === 0 && loc.y === 0) {
      return type === 'd' || type === 'e'
    }

    const up = downColor(this.tileAt(loc.up))
    const down = upColor(this.tileAt(loc.down))
    const left = rightColor(this.tileAt(loc.left))
    const right = leftColor(this.tileAt(loc.right))
    return (
      (up || down || left || right) &&
      (!up || up === upColor(type)) &&
      (!down || down === downColor(type)) &&
      (!left || left === leftColor(type)) &&
      (!right || right === rightColor(type))
    )
  }

  /** Get a list of possible tiles that can be played in this location.
   * @arg {Point} loc - the location to check
   * @arg {string | boolean} s=false - if provided, a direction modifier, '/', '\\', or '+'
   * @returns {TileType[]}
   */
  possibleTiles(loc, s = false) {
    /** @type TileType[] */
    const possibles = []
    for (const t of tileTypes) {
      if ((!s || s === slash[t]) && this.validTile(t, loc)) possibles.push(t)
    }

    return possibles
  }

  get height() {
    return this.bottom - this.top + 1
  }

  get width() {
    return this.right - this.left + 1
  }

  validLocation(loc) {
    if (this.over) return false
    if (this.tileAt(loc)) return false
    if (this.count === 0 && loc.eq(zero)) return true
    if (this.rules === 'trax8') {
      if (this.width > 7 && (loc.x < this.left || loc.x > this.right)) {
        return false
      }

      if (this.height > 7 && (loc.y < this.top || loc.y > this.bottom)) {
        return false
      }
    }

    for (const location of loc.around) {
      if (this.tileAt(location)) return true
    }

    return false
  }

  possibleLocations() {
    if (this.count === 0) return [zero]
    /** @type Record<TileId, Point> */
    const possibles = {}

    for (const tile of Object.values(this.tiles)) {
      tile.loc.around.map((loc) => {
        const id = this.tileId(loc)
        if (!(id in this.tiles)) possibles[id] = loc
        return loc
      })
    }

    // Get rid of all the real tiles and return all the maybes
    return Object.values(possibles)
  }

  possibleMoves() {
    // Returns a list of every currently possible move
    // as notations, ie ['@1+', '@1/', '@1\\', ...]
    const possibles = []
    if (this.over) return possibles // Shortcut if the game is over
    for (const loc of this.possibleLocations()) {
      for (const tile of this.possibleTiles(loc)) {
        const drop = this.dropTile(tile, loc, 'tentative')
        if (drop.valid) possibles.push(drop.notation)
      }
    }

    return possibles
  }

  forcedMoves(loc) {
    // We just played at loc, so return all forced moves
    const forced = []
    const invalids = {}
    let check = loc.around
    while (check.length > 0) {
      const pos = check.shift()
      if (this.validLocation(pos)) {
        const possibles = this.possibleTiles(pos)
        if (possibles.length === 1) {
          // Only 1 possible move
          forced.push(this.addTile(possibles[0], pos))
          check = [...check, ...pos.around]
        } else if (possibles.length === 0) {
          // No possible moves at all
          const invalid = this.addTile('x', pos)
          if (!invalids[invalid.id]) forced.push(invalid)
          invalids[invalid.id] = true
        }
      }
    }

    return forced
  }

  notate(type, loc) {
    // Notation depends on the position of the board BEFORE the move
    let notation = Trax.encodeCol(loc.x - this.left + 1)
    notation += String(loc.y - this.top + 1)
    notation += slash[type]
    return notation
  }

  decodeNotation(notation) {
    // Return {type, loc} from a notation
    const bad = { type: 'x', loc: zero }
    const match = notation.match(notationRegex)
    if (match === null) return bad
    const loc = new Point(
      this.left - 1 + Trax.decodeCol(match[1]),
      this.top - 1 + Number(match[2]),
    )
    const possible = this.possibleTiles(loc, match[3])
    if (possible.length > 0) return { type: possible[0], loc }
    return bad
  }

  follow(color, loc, from) {
    const path = []
    let id = this.tileId(loc)
    let tile = this.tileAt(loc)
    while (tile && path[0] !== id) {
      if (from !== 'top' && upColor(tile) === color) {
        from = 'bottom'
        loc = loc.up
      } else if (from !== 'bottom' && downColor(tile) === color) {
        from = 'top'
        loc = loc.down
      } else if (from !== 'left' && leftColor(tile) === color) {
        from = 'right'
        loc = loc.left
      } else if (from !== 'right' && rightColor(tile) === color) {
        from = 'left'
        loc = loc.right
      }

      path.push(id)
      id = this.tileId(loc)
      tile = this.tileAt(loc)
    }

    return { loc, path }
  }

  findEnds(color, loc) {
    const ends = []
    const tile = this.tileAt(loc)
    if (upColor(tile) === color) ends.push(this.follow(color, loc, 'top'))
    if (downColor(tile) === color) ends.push(this.follow(color, loc, 'bottom'))
    if (leftColor(tile) === color) ends.push(this.follow(color, loc, 'left'))
    if (rightColor(tile) === color) ends.push(this.follow(color, loc, 'right'))
    return ends
  }

  lineWin(locA, locB) {
    if (this.rules === 'traxloop') return false
    if (this.width > 7 && locA.distX(locB) > this.width) return true
    if (this.height > 7 && locA.distY(locB) > this.height) return true
    return false
  }

  checkWin(tiles) {
    /** @type {Color|false} */
    let winner = false
    const colors = [this.color, Trax.other(this.color)] // Check win for current player first
    for (const color of colors) {
      if (winner) continue // Skip the second player if the first has won
      for (const tile of tiles) {
        if (winner) continue // Skip the remaining tiles once we find a win
        const ends = this.findEnds(color, tile.loc)
        if (ends[0].loc.eq(ends[1].loc)) {
          winner = color // Loop win
          this.path = ends[0].path // Both paths are essentially the same
        } else if (this.lineWin(ends[0].loc, ends[1].loc)) {
          winner = color // Line win, paths need to be combined
          this.path = [...ends[0].path.reverse(), ...ends[1].path.slice(1)]
        }
      }
    }

    if (winner) {
      this.over = true
      this.turn = Trax.playerNumber(winner)
    } else if (
      // 8x8 Trax can end in a tie once there are no valid moves left
      this.rules === 'trax8' &&
      this.width > 7 &&
      this.height > 7 &&
      this.possibleMoves().length === 0
    ) {
      this.over = true
      this.turn = 0 // Tie
    }
  }

  /** Play a move. Can be called either as:
   * - play(moveNumber, notation) to ensure move safety, or
   * - play(notation) for quicker access.
   * @arg moveNumber {(number|string)} the move number or the notation
   * @arg {string} [notation] - the notation, if a move number was provided
   * @returns {{dropped: Tile[], notation: string, valid: boolean}}
   */
  play(moveNumber, notation) {
    // Generic API call for playing a move with move number safety
    // or, alternately, pass only a single value, notation, for a quick play
    if (notation) {
      if (moveNumber !== this.move + 1) {
        return { dropped: [], notation: '', valid: false }
      }
    } else {
      notation = String(moveNumber)
    }

    return this.dropTile(notation)
  }

  playMoves(moves) {
    // Play one or more moves, from a string or a list of moves
    // If move numbers are included, ensure they are accurate
    if (Array.isArray(moves)) {
      moves = moves.join(' ')
    }

    moves = moves.replace(/\n/g, ' ').split(/\s+/)

    let moveNumber = 0
    for (const move of moves) {
      if (moveNumberRegex.test(move)) {
        moveNumber = Number(move.replace(/\D/g, ''))
      }

      if (notationRegex.test(move)) {
        if (moveNumber) {
          this.play(moveNumber, move)
        } else {
          this.play(move)
        }

        moveNumber = 0
      }
    }
  }

  updateNotation(notation) {
    this.move++
    let note = this.move + '. ' + notation
    const line = this.notation.split('\n').pop()
    if (line) {
      note = (line.length + note.length > 79 ? '\n' : ' ') + note
    }

    this.notation += note
  }

  /** An array of the moves made in the game. */
  get moves() {
    return this.notation
      .replace(/\n/g, ' ')
      .split(' ')
      .filter((n) => Boolean(n) && !n.endsWith('.'))
  }

  /** Drop a tile onto the board. This is a lower level call. Use play() if possible instead.
   * @arg {string|TileType} type - a special move, a tile type, or a notation
   * @arg {Point} [loc] - a location if type is a tile type
   * @arg {string|boolean} [tentative] - if truthy, the move will not be saved
   * @returns {TileDrop} an object representing the results of the drop
   */
  dropTile(type, loc, tentative) {
    /** @type Tile[] */
    let dropped = []
    let notation = ''
    let valid = false
    if (['timeout', 'resign', 'draw', 'puzzled'].includes(type)) {
      // Handle special events
      this.over = true
      if (type !== 'puzzled') {
        // Puzzled: puzzle was not completed; this player wins
        this.turn = this.turn === 1 ? 2 : 1 // Timeout, resign means the other player wins
        if (type === 'draw') this.turn = 0 // Draw means we both win
      }

      this.updateNotation(type)
      notation = type
      valid = true
    } else {
      if (type.search(notationRegex) === 0) {
        // We were passed a notation
        const decoded = this.decodeNotation(type)
        type = decoded.type
        loc = decoded.loc
      }

      if (loc && this.validLocation(loc) && this.validTile(type, loc)) {
        valid = true
        const saved = this.save() // Just in case we need to roll back
        notation = this.notate(type, loc) // Determine notation BEFORE we play the move
        this.updateNotation(notation)
        dropped.push(this.addTile(type, loc)) // Play the tile
        dropped = [...dropped, ...this.forcedMoves(loc)] // Play all forced moves
        if (this.invalid || tentative) {
          if (this.invalid) valid = false
          this.restore(saved) // Abort! abort!
        } else {
          this.checkWin(dropped)
          if (!this.over) this.turn = this.turn === 1 ? 2 : 1
        }
      }
    }

    return { dropped, notation, valid }
  }

  moveRotations(move) {
    // Symmetry helper, rotate a move around the board
    const match = move.match(notationRegex)
    if (match === null) return []
    const x = Trax.decodeCol(match[1])
    const y = Number(match[2])
    const X = this.height - x + 1
    const Y = this.width - y + 1
    const s = match[3]
    const slashes = s === '+' ? [s] : ['/', '\\']
    const moves = new Set()
    // This isn't super precise, but it limits the search space ... maybe #TODO?
    for (const slash of slashes) {
      moves.add(Trax.encodeCol(x) + String(y) + slash)
      moves.add(Trax.encodeCol(y) + String(x) + slash)
      moves.add(Trax.encodeCol(X) + String(Y) + slash)
      moves.add(Trax.encodeCol(Y) + String(X) + slash)
    }

    return [...moves]
  }

  provisionalMove(from, to, via) {
    // Test a provisional move against the current state
    let rows = from.slice(1).split(':')
    let cols = 0
    for (const row of rows) {
      cols = Math.max(cols, codeRowLength(row))
    }

    rows = rows.length
    if (
      (this.width > rows && this.height > cols) ||
      (this.width > cols && this.height > rows)
    ) {
      return 'delete-provisional' // Current board is bigger than the provisional move
    }

    if (from === this.normalized) {
      const saved = this.save() // Save this position
      for (const move of this.moveRotations(via)) {
        // Try all symmetrical moves
        const drop = this.dropTile(move)
        if (drop.valid) {
          if (to === this.normalized) return move // This move matched!
          this.restore(saved) // Put the board back how it was
        }
      }
    }

    return false
  }

  get icon() {
    return this.positionCode()
  }

  dropsIcon(drops) {
    return this.positionCode(undefined, undefined, undefined, drops)
  }

  // Symmetry helper, draw the board from different angles
  positionCode(rightToLeft, bottomToTop, rotate, drops) {
    const startX = rightToLeft ? this.right : this.left
    const startY = bottomToTop ? this.bottom : this.top
    const incX = rightToLeft ? -1 : 1
    const incY = bottomToTop ? -1 : 1
    const code = []
    let line = ''
    let x = startX
    let y = startY
    do {
      const loc = new Point(x, y)
      const tile = this.tileAt(loc) // Normal tile for normal orientation
      if (tile) {
        /** @type string */
        let encoded = applySymmetry(tile, rightToLeft, bottomToTop, rotate)
        if (this.path.includes(this.tileId(loc))) {
          encoded = String.fromCodePoint(encoded.codePointAt(0) + this.turn * 6)
        }

        if (
          drops &&
          drops.dropped &&
          drops.dropped.some((t) => loc.eq(t.loc))
        ) {
          encoded = encoded.toUpperCase()
        }

        line += encoded
      } else {
        line = addBlank(line)
      }

      if (rotate) {
        // Increment y more often than x
        y += incY
        if (y < this.top || y > this.bottom) {
          y = startY
          x += incX
          // Remove final number and add to code
          code.push(line.replace(/\d+$/, ''))
          line = ''
        }
      } else {
        // This is the standard order
        x += incX
        if (x < this.left || x > this.right) {
          x = startX
          y += incY
          // Remove final number and add to code
          code.push(line.replace(/\d+$/, ''))
          line = ''
        }
      }
    } while (
      x >= this.left &&
      x <= this.right &&
      y >= this.top &&
      y <= this.bottom
    )

    if (rotate) code.reverse()
    return code.join(':')
  }

  normalize() {
    // Trax has the potential for symmetry, so this gives us the ability to
    // examine horizontal, vertical, and rotational symmetry for a color
    let norm = 'z'
    const bools = [true, false]
    for (const rightToLeft of bools) {
      for (const bottomToTop of bools) {
        for (const rotate of bools) {
          const code = this.positionCode(rightToLeft, bottomToTop, rotate)
          if (code < norm) norm = code
        }
      }
    }

    return (this.color || 't').toUpperCase() + norm
  }

  get normalized() {
    return this.normalize()
  }
}
