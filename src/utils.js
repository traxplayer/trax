/** Shared utilities
 * @copyright 2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** Convert milliseconds to a human readable string in ms, sec, or min units.
 * @arg {number} ms - the number of milliseconds
 * @returns {string} a short human-readable representation of the time
 */
export const timeString = (ms) => {
  if (ms < 1000) {
    return (ms === Math.floor(ms) ? String(ms) : ms.toFixed(2)) + ' ms'
  }

  ms /= 1000
  if (ms < 60) return ms.toFixed(3) + ' sec'

  const min = Math.floor(ms / 60)
  ms -= min * 60
  return String(min) + ' min ' + ms.toFixed(3) + ' sec'
}
