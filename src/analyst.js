/** Analyst module: provides analysis functions for Trax games.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import { Point } from '@slugbugblue/trax/point'
import { Trax } from '@slugbugblue/trax'
import { LRU } from '@slugbugblue/trax/lru'

import { threats } from '@slugbugblue/trax/threats'

const cache = new LRU(2750)

/** Helper function to generate a unique id for the analysis. */
const id = () => {
  const chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
  let code = ''
  while (code.length < 6) {
    code += chars[Math.floor(Math.random() * chars.length)]
  }

  return 'analysis-' + code
}

/** A sort function for hashes with a numeric score property.
 * @arg {PositionScore} a - The first Position Score to be compared
 * @arg {PositionScore} b - The second Position Score to be compared
 * @returns {number} a.score <=> b.score
 */
const byScoreDesc = (a, b) => b.score - a.score

/** A filter function factory to remove scores not within x points of the
 * highest score.
 * @arg highest {number} - The highest score
 * @arg tolerance {number} - How close another score must be to be included
 * @returns {(s:PositionScore)=>boolean} a filter function
 */
const withinX = (highest, tolerance) => (s) =>
  s.score === highest || highest - s.score <= tolerance

/** Return a random element of an array.
 * @template T
 * @arg {T[]} array - an array of elements
 * @returns {T} one of the elements at random
 */
const randomDraw = (array) => array[Math.floor(Math.random() * array.length)]

// Make parseInt a little shorter
const int = (s) => Number.parseInt(s, 10)

const leftTurn = { d: 'r', l: 'd', r: 'u', u: 'l' }
const rightTurn = { d: 'l', l: 'u', r: 'd', u: 'r' }
const follow = { d: 'right', l: 'bottom', r: 'top', u: 'left' }

const edgeColor = (tile, dir) => {
  return { d: 'ace', l: 'abd', r: 'bcf', u: 'def' }[dir].includes(tile)
    ? 'w'
    : 'b'
}

const encodeNumber = (n) => String.fromCodePoint(48 + n)

/** Explore a raw edge and update cave information.
 * @arg {RawEdge} rawEdge - the raw edge information
 * @returns {RawEdge}
 */
const spelunk = (rawEdge) => {
  for (let index = 1; index < rawEdge.length - 1; index++) {
    if (rawEdge[index].t === 'C') {
      const blank = () => ({ x: 0, y: 0, c: '', t: '' })
      const previous = rawEdge[index - 1] || blank()
      const next = rawEdge[index + 1] || blank()
      if (next.t === 'c') next.t = 'C'
      if (previous.t === 'c') {
        previous.t = 'C'
        if (index > 1) index -= 2
      }
    }
  }

  for (const space of rawEdge) {
    if (space.t === 'c') space.t = 'n'
    if (space.t === 'C') space.t = 'c'
  }

  return rawEdge
}

/** Combine hollows into a single item in the raw edge array.
 * @arg {RawEdge} rawEdge
 * @returns {RawEdge}
 */
const hollowOut = (rawEdge) => {
  for (let index = 1; index < rawEdge.length - 1; index++) {
    const space = rawEdge[index]
    if (space.c === 'l') {
      Object.assign(space, rawEdge[index - 1], rawEdge[index + 1])
      space.c = 'l'
      rawEdge.splice(index + 1, 1)
      rawEdge.splice(index - 1, 1)
    }
  }

  return rawEdge
}

/** Supposedly deep nesting is bad, so we broke this out of "pairUp" for
 * "better" code, even though this is useless on its own ...
 * @arg {RawEdge} rawEdge
 * @arg {number} index
 * @arg {string} color
 * @pairs {Record<string, number>}
 */
const findPair = (rawEdge, index, color, pairs) => {
  const colors = new Set([color, 'l'])
  const space = rawEdge[index]
  const nColor = 'n' + color
  for (const x of [1, 2]) {
    let j = x + index
    if (j >= rawEdge.length) j -= rawEdge.length
    const next = rawEdge[j]
    if (colors.has(next.c) && space[nColor] !== next[nColor]) {
      const one = color + space[nColor]
      const two = color + next[nColor]
      pairs[one + '-' + two] = index
      if (two + '-' + one in pairs) {
        for (const y of [index, pairs[two + '-' + one]]) {
          rawEdge[y]['p' + color] = true
        }
      }

      // Only process one connection
      break
    } else if (next.c === 'r') {
      // Cannot connect past an r space
      break
    }
  }

  for (let x = 1; x < rawEdge.length; x++) {
    let j = index + x
    if (j >= rawEdge.length) j -= rawEdge.length
    if (colors.has(rawEdge[j].c)) {
      space[color] = rawEdge[j][nColor] === space[nColor]
      break
    }
  }
}

/** Find connectable pairs in the raw edge.
 * @arg {RawEdge} rawEdge
 * @returns {RawEdge}
 */
const pairUp = (rawEdge) => {
  /** @type Record<string, number> */
  const pairs = {}
  for (const [index, space] of rawEdge.entries()) {
    for (const bw of ['w', 'b']) {
      const colors = [bw, 'l']
      if (colors.includes(space.c)) {
        findPair(rawEdge, index, bw, pairs)
      }
    }
  }

  return rawEdge
}

/** Label and finalize the raw edge.
 * @arg {RawEdge} rawEdge
 * @returns {EdgeObject}
 */
const label = (rawEdge) => {
  const final = { w: '', b: '', edge: rawEdge }
  let index = 0
  for (const space of rawEdge) {
    for (const bw of ['b', 'w']) {
      const colors = new Set([bw, 'l'])
      let z = 'w'
      if (colors.has(space.c)) {
        if (space['p' + bw]) z = 'c'
        if (space[bw]) z = 'a'
        if (space.c === 'l') z = { w: 'l', a: 'm', c: 'p' }[z]
      } else {
        z = space.c === 'r' ? 'r' : 'b'
      }

      if (space.t === 'x') z = 'x'
      space['z' + bw] = z
      final[bw] += z
    }

    space.idx = index
    index += 1
  }

  return final
}

/** Quick way to get the number of items in an array.
 * @returns {number} - length of the array
 */
const length = (array) => array?.length || 0

/** Calculate the points for the number of threats at a particular level.
 * @arg {ColorThreats} from - the threats found for a certain player
 * @arg {number} level - the level to calculate the points for
 * @arg {number} [as] - if included, the level the points should be calculated as
 * @return {number} the number of points for the level
 */
const attackScore = (from, level, as) => {
  const count = length(from[level])
  const calculateAs = Math.min(as === undefined ? level : as, threats.maxDepth)
  if (calculateAs < 1) return count
  return count * 10 ** (threats.maxDepth - calculateAs + 1)
}

/** Shortcut to get the number of attacks.
 * @arg {ColorThreats} from - the threats found for a certain player
 * @returns {number} the number of attacks (win on next move) present
 */
const attackCount = (from) => length(from['1'])

/** Shortcut to get the number of threats at a certain level.
 * @arg {ColorThreats} from - the threats found for a player
 * @arg {number} level - the level for the threats to look for
 * @returns {number} the number of threats at the given level
 */
const threatCount = (from, level) => length(from[level])

/** Shortcut to get the number of passive threats
 * @arg {ColorThreats} from - the threats found for a player
 * @returns {number} the number of passive threats (win in 2 or more moves)
 */
const passiveCount = (from) => {
  let count = 0
  for (const [depth, array] of Object.entries(from)) {
    count += int(depth) > 1 ? length(array) : 0
  }

  return count
}

/** Shortcut to determine if we have an active attack.
 * @arg {ColorThreats} from - the threats found for a player
 * @returns {boolean}
 */
const activeAttack = (from) =>
  attackCount(from) > 1 || (attackCount(from) === 1 && passiveCount(from) > 0)

/** Shortcut to determine if we have a passive attack.
 * @arg {ColorThreats} from - the threats found for a player
 * @returns {boolean}
 */
const passiveAttack = (from) => passiveCount(from) > 1

// /** Shortcut to determine if we are under attack.
//  * @arg {ColorThreats} from - the threats found for a player
//  * @returns {boolean}
//  */
// const underAttack = (from) => attackCount(from) > 0

/** Shortcut to determine if we are under threat.
 * @arg {ColorThreats} from - the threats found for a player
 * @returns {boolean}
 */
const underThreat = (from) => attackCount(from) + passiveCount(from) > 0

// Analysis class
export class Analysis {
  /** @type {TraxVariant} */
  #rules = 'trax'
  #id = 'analysis'
  /** @type {string[]} - List of moves to arrive at this position */
  #moves = []
  #notation = ''
  #edge = undefined
  /** @type {Trax} */
  #game
  #scores = {}
  #threats = undefined
  #faulty = { b: [], w: [] }
  #partial = false

  constructor(game, partial = false) {
    // Make a copy of the game
    this.#rules = game.rules
    this.#moves = game.moves
    this.#id = id()
    this.#notation = game.notation
    this.#game = new Trax(this.#rules, this.#moves, this.#id)
    this.count = {
      w: (level) => this.threats.w[level]?.length || 0,
      b: (level) => this.threats.b[level]?.length || 0,
    }
    this.#partial = Boolean(partial)
    if (this.#moves.length === 0 || game.over) {
      this.#edge = { b: '', w: '', rawEdge: [] }
      this.#threats = { b: {}, w: {} }
    }
  }

  get game() {
    // Return a fresh copy of the game as presented on init
    if (this.#game.notation !== this.#notation) {
      this.#game = new Trax(this.#rules, this.#moves, this.#id)
    }

    return this.#game
  }

  get edge() {
    if (!this.#edge) {
      // Cache the edge
      this.#edge = this.#traceEdge()
    }

    return this.#edge
  }

  /** Any faulty threats
   * @returns {FaultyThreats}
   */
  get faulty() {
    return this.#faulty
  }

  /** All threats present in this position.
   * @returns {FoundThreatsCollection}
   */
  get threats() {
    if (!this.#threats) {
      this.#threats = { b: this.#applyThreats('b'), w: this.#applyThreats('w') }
      if (!this.#partial) this.#validateThreats()
    }

    return this.#threats
  }

  // Is a given location in a cave?
  inCave(loc) {
    const { game } = this

    if (game.tileAt(loc)) {
      return false
    }

    const topLeft = new Point(game.left, game.top)
    const bottomRight = new Point(game.right, game.bottom)
    let count = 0

    for (const dir of Point.dirs) {
      let look = loc.dir(dir)
      while (look.in(topLeft, bottomRight) && !game.tileAt(look)) {
        look = look.dir(dir)
      }

      if (game.tileAt(look)) count++
    }

    return count > 2
  }

  /** Determine the points of the currently active player.
   * @returns {[number, number]} - the points for this player, and the level
   * of the most dangerous threat
   */
  #activeScore() {
    let score = 0
    let threatLevel = Number.POSITIVE_INFINITY
    const { game } = this
    const active = this.threats[game.color]

    // First examine the active player
    for (const level of Object.keys(active).map((i) => int(i))) {
      if (level > 0) threatLevel = Math.min(threatLevel, level)
      score += attackScore(active, level)
    }

    return [score, threatLevel]
  }

  /** Determine the points of the waiting player.
   * @arg {Color} color - the color for which we are generating the score
   * @arg {number} threatLevel - the lowest level for which the active player
   * has an active threat
   * @returns {[number, number]} - the points for this player and the multiplier
   * to be applied to the other player's points
   */
  #inactiveScore(color, threatLevel) {
    const { game } = this
    const active = this.threats[game.color]
    const inactive = this.threats[Trax.other(game.color)]

    // Is the color we are looking at the passive player?
    const passive = game.color !== color

    // Is the inactive player activating known threats?
    const attacking = activeAttack(inactive)

    // Was the inactive player unable to clear all threats?
    const defending = !attacking && underThreat(active)

    // Does the inactive player have multiple threats
    const threatening = !attacking && passiveAttack(inactive)

    // If calc is false, attacks and threats are treated as corners
    const calc = !passive || attacking || defending || threatening

    // If the current player does not hold the initiative, reduce the score
    const multiplier = passive && attacking && threatLevel > 1 ? 0.1 : 1

    let score = 0

    // Now calculate the effects of the passive player
    for (const level of Object.keys(inactive).map((i) => int(i))) {
      const calcLevel = level > 0 && defending ? level + threatLevel - 1 : level
      let points = attackScore(inactive, level, calc ? calcLevel : 0)
      if (level === threatLevel) points = passive ? Math.floor(points / 2) : 0
      if (passive && threatening) points = Math.floor(points / 2)
      score += points
    }

    return [score, multiplier]
  }

  #score(color) {
    if (this.#scores[color] === undefined) {
      const { game } = this
      if (game.over) {
        this.#scores[game.color] = Number.POSITIVE_INFINITY
        this.#scores[Trax.other(game.color)] = Number.NEGATIVE_INFINITY
        return this.#scores[color]
      }

      const [currentPlayer, threatLevel] = this.#activeScore()
      const [nextPlayer, multiplier] = this.#inactiveScore(color, threatLevel)

      let score = Math.floor(currentPlayer * multiplier) - nextPlayer
      if (score && game.color !== color) score *= -1

      this.#scores[color] = score
    }

    return this.#scores[color]
  }

  get scores() {
    return {
      w: this.#score('w'),
      b: this.#score('b'),
    }
  }

  get score() {
    return this.scores[this.game.color]
  }

  // Space type
  //
  // determine which type of space we have:
  // c = in a cave but has no restrictions (converted to n in edge)
  // C = a cave where some moves are not allowed (converted to c in edge)
  // n = normal space
  // x = no tile can be played here
  spaceType(loc) {
    const { game } = this

    if (!game.validLocation(loc)) {
      return 'x'
    }

    if (!this.inCave(loc)) {
      return 'n'
    }

    // Check to see if we have an invalid move
    let someValid = false
    let someInvalid = false
    for (const tile of game.possibleTiles(loc)) {
      const drop = game.dropTile(tile, loc, 'tentative')
      if (drop.valid) {
        someValid = true
      } else {
        someInvalid = true
      }
    }

    if (!someValid) {
      return 'x'
    }

    if (someInvalid) {
      return 'C'
    }

    return 'c'
  }

  // Trace the edge of the playing area
  #traceEdge() {
    const { game } = this
    /** @type {RawEdge} */
    const rawEdge = []
    const lines = {}
    const count = { w: 0, b: 0 }
    let start = new Point(game.left, game.top)
    let dir = 'r'

    while (!game.tileAt(start)) {
      start = start.right
    }

    let loc = start

    while (game.move > 0 && !game.over) {
      let space = loc.dir(leftTurn[dir])
      if (rawEdge.length > 0 && space.eq(rawEdge[0])) break

      const color = edgeColor(game.tileAt(loc), leftTurn[dir])
      const end = game.follow(color, loc, follow[dir]).loc
      const type = this.spaceType(space)
      const tile = { x: space.x, y: space.y, c: color, t: type }
      const id = color + end.x + ':' + end.y
      const nColor = 'n' + color

      if (id in lines) {
        tile[nColor] = lines[id][nColor]
      } else {
        tile[nColor] = encodeNumber(count[color])
        count[color]++
      }

      lines[color + space.x + ':' + space.y] = tile
      rawEdge.push(tile)

      const next = loc.dir(dir)

      if (game.tileAt(next)) {
        loc = next.dir(leftTurn[dir])
        if (game.tileAt(loc)) {
          rawEdge.push({ x: space.x, y: space.y, c: 'l', t: type })
          dir = leftTurn[dir]
        } else {
          loc = next
        }
      } else {
        space = next.dir(leftTurn[dir])
        const t = this.inCave(space) ? 'c' : 'n'
        rawEdge.push({ x: space.x, y: space.y, c: 'r', t })
        dir = rightTurn[dir]
      }
    }

    return label(pairUp(hollowOut(spelunk(rawEdge))))
  }

  /** Compose the threat into a FoundThreat object.
   * @arg {Threat} threat - the threat that has been found
   * @arg {RegExpMatchArray} match - the matching item
   * @returns {FoundThreat}
   */
  #foundThreat(threat, match) {
    return {
      threat: threat.pattern,
      match: match[1], // The match is in the capturing group
      at: match.index,
      value: threat.value,
      level: threat.depth,
    }
  }

  /**
   * @arg {FoundThreat} threat - a threat
   * @arg {string} color - the player color
   * @returns {Point[]} - locations relevant to the threat
   */
  threatLocs(threat, color) {
    const { edge } = this.edge
    const max = edge.length
    let index = threat.at
    let count = 0
    const locs = new Set([index])
    while (count < threat.match.length) {
      index += 1
      if (index >= max) index -= max
      locs.add(index)
      count += 1
      // Check to see if we have a connectable pair
      if (edge[index]['p' + color]) {
        const line = 'n' + color
        const end = edge.find(
          (space) =>
            space.idx !== edge[index].idx && space[line] === edge[index][line],
        )
        if (end) {
          // Add the opposite end and the space right before it, just in case
          locs.add(end.idx)
          locs.add(end.idx - 1)
        }
      }
    }

    return [...locs].map((i) => new Point(edge[i] || { x: 0, y: 0 }))
  }

  /** Apply the threats to a color's regex-ready edge.
   * @arg {Color} color - the color to find threats for
   * @returns {ColorThreats}
   */
  #applyThreats(color) {
    /** @type {ColorThreats} */
    const matches = {}
    const { length } = this.edge[color]
    const edge = this.edge[color].repeat(2)
    for (const threat of threats.list) {
      const find = matches[threat.depth] || []
      for (const match of edge.matchAll(threat.rx)) {
        if (match.index >= length) break

        // Avoid saving duplicates
        if (find.every((f) => f.at !== match.index || f.match !== match[1])) {
          find.push(this.#foundThreat(threat, match))
        }
      }

      if (find.length > 0) matches[threat.depth] = find
    }

    return matches
  }

  /** Clean up the list of matches, removing any false positives. */
  #validateThreats() {
    for (const bw of ['w', 'b']) {
      /** @type ColorThreats */
      const matches = this.#threats[bw]
      for (const key of Object.keys(matches)) {
        const level = Number(key)
        if (level < 2) continue
        /** @type {FoundThreat[]} */
        const verified = []
        const threats = matches[key]
        for (const threat of threats) {
          if (this.#canActivateThreat(threat, bw, level)) {
            verified.push(threat)
          } else {
            this.#faulty[bw].push(threat)
          }
        }

        if (verified.length > 0) {
          matches[key] = verified
        } else {
          delete matches[key]
        }
      }
    }
  }

  /** Determine if the given threat can be activated for this color
   * @param {FoundThreat} threat - The threat to try to activate
   * @param {string} color - The color this match belongs to
   * @param {number} level - The depth level of this threat
   * @returns {boolean}
   */
  #canActivateThreat(threat, color, level) {
    const attacks = attackCount(this.#threats[color])
    const player = Trax.playerNumber(color)
    const other = Trax.other(color)
    const otherAttacks = attackCount(this.#threats[other])
    const higher = threatCount(this.#threats[color], level - 1)
    for (const loc of this.threatLocs(threat, color)) {
      for (const type of this.game.possibleTiles(loc)) {
        const game = new Trax(this.game.rules, this.game.moves)
        const result = game.dropTile(type, loc)
        if (!result.valid) continue

        // This might be a bad shortcut, but if we already have an attack here
        // we just assume that this threat is also valid ...
        if (game.over && game.turn === player) return true

        // Otherwise, we cannot evaluate an ended game
        if (game.over) continue

        const { threats } = analyze(game, true)

        // If we give our opponent an attack, this is a faulty move
        const faulty = attackCount(threats[other]) - otherAttacks
        if (faulty > 0) continue

        // Activating the threat means we get an attack and a threat
        const newAttacks = attackCount(threats[color]) - attacks
        if (level === 2) {
          if (newAttacks > 1) return true
        } else {
          const newThreats = threatCount(threats[color], level - 1) - higher
          if (newAttacks > 0 && newThreats > 0) return true
        }
      }
    }

    return false
  }

  // Make a reasonable inspect representation of the analysis
  [Symbol.for('nodejs.util.inspect.custom')](depth, options, inspect) {
    const sub = (object) => inspect(object, options).replace(/\n/g, '\n  ')
    const style = options.stylize
    if (depth < 0) return style('<Analysis>', 'special')
    if (options.depth !== null) options.depth -= 1
    const tbd = style('tbd', 'undefined')
    let custom = style('Analysis', 'special') + ' {\n'
    custom += '  game: ' + style(this.#rules, 'special')
    custom += '(' + style(this.#moves, 'string') + '),\n'
    custom += '  edge: '
    if (this.#edge) {
      custom += '{\n    w: ' + style(this.#edge.w, 'string')
      custom += ',\n    b: ' + style(this.#edge.b, 'string')
      custom += ',\n    edge: ' + style('[rawEdge]', 'special')
      custom += '\n  }'
    } else {
      custom += tbd
    }

    custom += ',\n  threats: '
    custom += this.#threats ? sub(this.#threats) : tbd
    custom += ',\n  scores: '
    custom += 'w' in this.#scores ? sub(this.#scores) : tbd
    custom += ',\n  score: '
    const score = 'w' in this.#scores ? this.score : null
    custom += score === null ? tbd : style(score, 'number')

    return custom + '\n}'
  }
}

/** Analyze a game position.
 * @arg {Trax} game - the position to analyze
 * @arg {boolean} partial - if the position should only be partially analyzed
 * @returns {Analysis}
 */
export const analyze = (game, partial = false) => {
  const id = (partial ? 'y' : 'n') + game.rules + game.normalized
  const cached = cache.get(id)
  if (cached) return cached
  const analysis = new Analysis(game, partial)
  cache.set(id, analysis)
  return analysis
}

export class Suggestion {
  /** @type {PositionScore[]} */
  all = []
  /** @type {PositionScore[]} */
  options = []
  /** @type {PositionScore} */
  pick
  ms = 0

  constructor(analyzed, ms) {
    this.all = analyzed.sort(byScoreDesc)
    const best = analyzed[0] || { move: '', score: 0 }
    this.options = analyzed.filter(withinX(best.score, 5))
    this.pick = randomDraw(this.options) || best
    this.ms = ms
  }

  get best() {
    return this.all[0]
  }

  get analyzed() {
    return this.all.length
  }

  // Make a nice representation for the nodejs REPL
  [Symbol.for('nodejs.util.inspect.custom')](_, options) {
    const style = options.stylize
    let out = style(this.pick.move, 'string')
    out += ' ' + style(this.pick.score, 'number') + ' ['
    out += this.options
      .slice(0, 5)
      .map((s) => style(s.move, 'string') + ' ' + style(s.score, 'number'))
      .join(', ')
    out += '] ' + this.analyzed + ' in ' + this.ms + 'ms'
    return out
  }
}

export const suggest = (game, partial = false) => {
  const start = Date.now()
  const analyzed = {}
  const { color, moves, rules } = game
  for (const move of game.possibleMoves()) {
    const trax = new Trax(rules, moves)
    trax.play(move)
    const unique = trax.normalized
    if (!analyzed[unique]) {
      const analysis = analyze(trax, partial)
      analyzed[unique] = { move, score: analysis.scores[color], analysis }
    }
  }

  return new Suggestion(Object.values(analyzed), Date.now() - start)
}
