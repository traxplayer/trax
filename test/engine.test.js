/* Copyright 2021-2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import test from 'ava'
import { Trax } from '@slugbugblue/trax'

//  Trax tiles (bottom: white wins / black wins)
//      a         b         c         d        e         f
//  ________  ________  ________  ________  _______  ________
//  |__##  |  |__##__|  |  ##__|  |_/ |  |  | | | |  |  | \_|
//  |_ \###|  |______|  |###/ _|  |__/###|  |#| |#|  |###\__|
//  |_\_|__|  |__##__|  |__|_/_|  |__##__|  |_|_|_|  |__##__|
//
//    g  m      h  n      i  o      j  p      k q      l  r

test('play trax game', (t) => {
  const trax = new Trax('trax', undefined, 'tap')
  t.is(trax.id, 'tap', 'id is assigned')
  t.is(trax.name, 'Trax', 'has the correct name')
  t.is(trax.count, 0, 'starts with an empty board')
  t.is(trax.turn, 1, 'player 1 starts')
  t.is(trax.color, 'w', 'player 1 is white')
  let play = trax.dropTile('A1/')
  t.false(play.valid, 'cannot play illegal move')
  play = trax.dropTile('@0/')
  const mess = JSON.stringify({
    x: play.dropped[0].loc.x,
    y: play.dropped[0].loc.y,
  })
  t.is(trax.dropsIcon(play), 'D', `drops icon from ${mess} is correct`)
  t.true(play.valid, 'can play first move')
  t.is(play.notation, '@0/', 'should have correct notation')
  t.is(play.dropped.length, trax.count, 'dropped tiles were played')
  t.is(trax.count, 1, '1 tile is on the board')
  t.false(trax.over, 'game is still in progress')
  t.is(trax.turn, 2, 'player 2 is next')
  t.is(trax.color, 'b', 'player 2 is black')
  t.is(trax.icon, 'd', 'icon shows correct tiles')
  trax.dropTile('A0\\')
  t.is(trax.count, 2, 'can play second move')
  t.false(trax.over, 'game is still in progress')
  t.is(trax.turn, 1, 'player 1 is next')
  t.is(trax.color, 'w', 'player 1 is white')
  t.is(trax.icon, 'a:d', 'icon shows correct tiles')
  trax.dropTile('@1/')
  t.is(trax.count, 4, 'forced moves work')
  t.true(trax.over, 'game ends on white loop')
  t.is(trax.winner, 1, 'player 1 wins')
  t.is(trax.path.length, 4, 'loop is over all tiles')
  t.is(
    trax.notation,
    '1. @0/ 2. A0\\ 3. @1/',
    'game should be notated correctly',
  )
  t.is(trax.moves.length, 3, 'three moves were made in the game')
  t.is(trax.icon, 'ig:lj', 'icon shows winning tiles')
})

test('starting notation', (t) => {
  let trax = new Trax('trax', '@0/ a0+ b2+')
  t.is(trax.moves.length, 3, 'lower-case notation')
  trax = new Trax('trax', '1. @0/ 2. A0+ 3. B2+')
  t.is(trax.moves.length, 3, 'full notation')
  trax = new Trax('trax', '@0/\na0+\nb2+\n')
  t.is(trax.moves.length, 3, 'multi-line notation')
  trax = new Trax('trax', 'z17+ a5/ @0/ b9\\ A0+ c7/ b2+')
  t.is(trax.moves.length, 3, 'ignore illegal moves')
  trax = new Trax('trax', 'start with @0/ then play a0+ and finally go for b2+')
  t.is(trax.moves.length, 3, 'ignore extra fluff')
})

test('trax line win', (t) => {
  const trax = new Trax('trax', '@0+ A0+ A0+ A0+ A0+ A0+ A0+', 'tap')
  t.is(trax.id, 'tap', 'id is assigned')
  t.is(trax.count, 7, '7 tiles played')
  t.is(trax.turn, 2, 'player 2 is next')
  t.is(trax.color, 'b', 'player 2 is black')
  t.is(trax.icon, 'e:e:e:e:e:e:e', 'icon shows correct tiles')
  trax.dropTile('A0+')
  t.true(trax.over, 'game has ended')
  t.is(trax.winner, 1, 'player 1 wins')
  t.is(trax.color, 'w', 'white wins')
  t.is(trax.count, 8, '8 tiles played')
  t.is(trax.path.length, 8, 'line win goes through all tiles')
  t.is(trax.icon, 'k:k:k:k:k:k:k:k', 'icon shows winning tiles')
})

test('dead cave', (t) => {
  const trax = new Trax('trax', '@0/ B1/ @1+ A2/ A3+ A4/ B4+ C4/')
  t.is(trax.count, 8, 'cave is created')
  const drop = trax.play('c3/')
  t.false(drop.valid, 'illegal cave move is not valid')
})

test('lines in a trax loop game', (t) => {
  const loop = new Trax('traxloop', '@0+ A0+ A0+ A0+ A0+ A0+ A0+ A0+', 'tap')
  t.is(loop.rules, 'traxloop', 'rules are trax loop')
  t.is(loop.name, 'Loop Trax', 'name is Loop Trax')
  t.is(loop.count, 8, '8 tiles were played')
  t.false(loop.over, 'game is not over')
  t.is(loop.icon, 'e:e:e:e:e:e:e:e', 'icon shows correct tiles')
  const play = loop.dropTile('B4/')
  t.true(play.valid, 'we can play another tile')
  t.is(loop.count, 10, 'two new tiles were played')
  t.false(loop.over, 'the game is still going')
  t.is(loop.turn, 2, 'player 2 is next')
  t.is(loop.color, 'b', 'player 2 is black')
  t.is(loop.icon, 'e:e:ef:ec:e:e:e:e', 'icon shows correct tiles')
  loop.dropTile('@4\\')
  t.is(loop.count, 12, 'two new tiles were played')
  t.true(loop.over, 'game ends with a loop')
  t.is(loop.winner, 2, 'player 2 wins')
  t.is(loop.icon, '0e:0e:pqr:mqo:0e:0e:0e:0e', 'icon shows winning tiles')
})

test('8x8 trax size limits', (t) => {
  const eight = new Trax('trax8', '@0+ @1/ c1/ D1+ e1/ @1+ G1+ H1\\', 'tap')
  t.is(eight.rules, 'trax8', 'rules are 8x8 trax')
  t.is(eight.name, '8x8 Trax', 'name is 8x8 Trax')
  t.is(eight.width, 8, 'board is 8 wide')
  let play = eight.dropTile('@1/')
  t.false(play.valid, 'cannot play on left')
  play = eight.dropTile('I1/')
  t.false(play.valid, 'cannot play on right')
  eight.play('h0+')
  t.is(eight.icon, '6e:bdecbdef', 'icon compresses multiple spaces')
  let plays = 0
  let moves = eight.possibleMoves()
  while (moves.length > 0) {
    const save = eight.save()
    for (const move of moves) {
      play = eight.play(move)
      if (play.valid) {
        if (!eight.gameOver) break
        if (eight.turn === 0) break
        eight.restore(save)
      }
    }

    plays++
    if (plays > 99) {
      t.fail('we got lost looking for a draw')
      break
    }

    moves = eight.possibleMoves()
  }

  t.is(eight.count, 64, '8x8 board fills up')
  t.is(eight.turn, 0, 'full playing area is a draw')
  t.true(eight.over, 'the game is over')
})

test('possibles', (t) => {
  const trax = new Trax()
  const locs = trax.possibleLocations()
  t.is(locs.length, 1, 'one location to play in')
  const zero = Trax.point(0, 0)
  const tiles = trax.possibleTiles(zero)
  t.is(tiles.length, 2, 'two possible tiles')
  for (const tile of tiles) {
    const drop = trax.dropTile(tile, zero, 'tentative')
    t.true(drop.valid, `${tile} should be a valid initial tile`)
    if (!drop.valid) {
      t.is(drop, { dropped: [], notation: '', valid: true }, 'this is broken')
    }
  }

  const possibles = trax.possibleMoves()
  t.is(possibles.length, 2, 'first move should have two possible moves')
})

test('normalizations and provisionals', (t) => {
  const one = new Trax('trax', '@0/', 'tap')
  const pre = one.normalized
  one.dropTile('a0+')
  const two = new Trax('trax', '@0/ @1+', 'tap')
  t.is(one.normalized, two.normalized, 'normalized positions are identical')
  const three = new Trax('trax', '@0/ B1+', 'tap')
  const four = new Trax('trax', '@0/ a2+', 'tap')
  t.is(three.normalized, four.normalized, 'normalized positions are identical')
  t.not(
    one.normalized,
    three.normalized,
    'opposite positions are not identical',
  )
  const from = one.normalized
  one.dropTile('A0\\')
  const to = one.normalized
  let play = two.provisionalMove(from, from, 'A0\\')
  t.false(play, 'unmatching provisional destination does not work')
  play = two.provisionalMove(from, to, 'A0\\')
  t.is(play, '@1\\', 'equivalent provisionals are found')
  play = two.provisionalMove(pre, to, 'a0+')
  t.false(play, 'unmatching provisional spaces return no move')
  three.dropTile('b0+')
  const old = three.provisionalMove(pre, to, 'A0\\')
  t.is(old, 'delete-provisional', 'old provisionals are deleted')
  play = four.provisionalMove(four.normalized, three.normalized, 'B0+')
  t.is(play, '@2+', 'straights also work')
  const five = new Trax('trax', '@0/ B1/')
  const six = new Trax('trax', '@0/ @1/')
  t.not(five.normalized, six.normalized, 'inverse colors are not the same')
})

test('seeking 100', (t) => {
  // @ts-expect-error
  let trax = new Trax('unknown variant')
  t.is(trax.rules, 'trax', 'default rules should be trax')
  t.false(trax.winner, 'no winner at the beginning of the game')
  trax.play(1, '@0/')
  t.is(trax.move, 1, 'second move')
  trax.play(1, '@0/')
  t.is(trax.move, 1, 'still second move')
  trax.dropTile('puzzled')
  t.true(trax.over, 'game ends after a puzzle loss')
  const zero = Trax.point(0, 0)
  t.false(trax.validLocation(zero), 'no valid locations when game is over')
  trax = new Trax('trax', '@0/ a2+')
  trax.dropTile('draw')
  t.true(trax.over, 'draw ends a game')
  t.is(trax.winner, 0, 'neither side wins')
  t.is(
    trax.normalized.slice(0, 1),
    'T',
    'tied games have a position code starting with T',
  )
  trax = new Trax()
  trax.play('resign')
  t.is(trax.winner, 2, 'player 2 wins if player 1 resigns')
  trax = new Trax('trax', '@0+')
  t.is(trax.decodeNotation('bob').type, 'x', 'cannot decode invalid notation')
  t.is(trax.decodeNotation('Q4+').type, 'x', 'notation outside of board')
  t.deepEqual(
    trax.moveRotations('bob'),
    [],
    'cannot rotate an invalid notation',
  )
  t.false(trax.validLocation(Trax.point(83, 45)), 'location outside of board')
  trax.play('timeout')
  t.is(trax.winner, 1, 'player 1 wins if player 2 times out')
})
