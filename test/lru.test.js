/** Testing the LRU code
 * @copyright 2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import { inspect } from 'node:util'
import test from 'ava'

import { LRU } from '@slugbugblue/trax/lru'

const error = { instanceOf: SyntaxError }

test('creation', (t) => {
  t.notThrows(() => new LRU(), 'default')
  t.notThrows(() => new LRU(100), 'specified capacity')
  t.throws(() => new LRU(0), error, 'zero')
  t.throws(() => new LRU(-100), error, 'negative')
})

test('LRU functions', (t) => {
  const cache = new LRU(5)
  t.is(cache.capacity, 5, 'capacity alias')
  cache.set('a', 1)
  cache.set('b', 2)
  cache.set('c', 3)
  cache.set('d', 4)
  cache.set('e', 5)
  t.deepEqual(
    [...cache.entries()],
    [
      ['a', 1],
      ['b', 2],
      ['c', 3],
      ['d', 4],
      ['e', 5],
    ],
    'map entries',
  )
  t.is(cache.has('a'), true, 'has true')
  t.is(cache.has('z'), false, 'has false')
  t.is(cache.size, 5, 'holds five items')
  cache.set('f', 6)
  t.is(cache.size, 5, 'expired one')
  t.deepEqual([...cache.keys()], ['b', 'c', 'd', 'e', 'f'], 'keys')
  t.is(cache.expired, 1, 'counting expirations')
  t.is(cache.get('b'), 2, 'retrieval')
  t.is(cache.hits, 1, 'counting hits')
  t.is(cache.get('a'), undefined, 'expired item is gone')
  cache.set('b', -2)
  cache.set('g', 7)
  t.is(cache.get('b'), -2, 'recent item saved')
  t.is(cache.get('c'), undefined, 'new oldest item deleted')
  t.deepEqual([...cache.values()], [4, 5, 6, 7, -2], 'values')
  t.is(cache.misses, 2, 'counting hits')
  t.is(cache.peek('d'), 4, 'peek returns correct value')
  cache.set('h', 8)
  t.is(cache.get('d'), undefined, 'peek does not bump the LRU for an item')
  cache.delete('f')
  t.is(inspect(cache), 'LRU(4 of 5)', 'inspection')
  t.is(cache.expired, 3, 'counting expirations')
  cache.clear()
  t.is(cache.size, 0, 'clearing cache')
  t.is(cache.toString(), 'LRU(0 of 5)', 'toString')
})
