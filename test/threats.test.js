/* Copyright 2022-2023 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import util from 'node:util'

import test from 'ava'

import { threats } from '@slugbugblue/trax/threats'

const INSPECT = '[Threats:362] { 0:7 1:6 2:60 3:264 4:21 5:3 6:1 }'
const ONE = '[Threat] { ArW: 0 }'
const REFORM = '[Threat] { rm?lr: 1 ! }'

test('counts', (t) => {
  t.is(util.inspect(threats), INSPECT, 'inspection')
  t.is(threats.level(0).length, 7, 'corners')
  t.is(threats.level(1).length, 6, 'attacks')
  t.is(threats.level(2).length, 60, 'ls')
  t.is(threats.level(3).length, 264, '3-stage')
  t.is(threats.level(4).length, 21, '4-stage')
  t.is(threats.level(5).length, 3, '5-stage')
  t.is(threats.level(6).length, 1, '6-stage')
  t.is(threats.list.length, 362, 'list')
  t.is(threats.maxDepth, 6, 'maximum depth')
  t.is(util.inspect(threats.list[0]), ONE, 'threat inspection')
  t.is(util.inspect(threats.list[9]), REFORM, 're-forming threat inspection')
})

test('patterns', (t) => {
  for (const threat of threats.list) {
    t.true('depth' in threat, 'has a depth')
    t.true('pattern' in threat, 'has a pattern')
    t.true('rx' in threat, 'has rx')
    t.true(threat.rx instanceof RegExp, 'rx is a regex')
    t.true(threat.pattern.length > 1, 'pattern length')
  }
})

test('no duplicates', (t) => {
  // Check that you can't directly add a duplicate
  t.throws(
    () => {
      // This threat already exists at depth 1
      threats.add(3, 'a?W')
    },
    { instanceOf: Error },
    'add a duplicate pattern',
  )
  // Check that the patterns don't match each other
  for (const one of threats.list) {
    for (const two of threats.list) {
      if (one === two) continue
      const match = one.rx.exec(two.pattern)
      if (match) {
        t.not(two.pattern, match[0], 'duplicate pattern')
      }
    }
  }
})
