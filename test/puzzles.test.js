/** Unit testing the puzzles.
 * @copyright 2022
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import test from 'ava'
import { puzzles } from '@slugbugblue/trax/puzzles'

test('no duplicates', (t) => {
  const unique = new Set()
  for (const puzzle of puzzles) {
    t.false(unique.has(puzzle.notation), `Puzzle ${puzzle.id} is unique`)
    unique.add(puzzle.notation)
  }
})
