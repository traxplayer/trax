/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { inspect } from 'node:util'
import test from 'ava'
import { Point } from '@slugbugblue/trax/point'

const log = (message) => {
  console.log(message)
}

test('basic points', (t) => {
  const a = new Point(12, 5)
  t.true(a.eq(new Point(12, 5)), 'equal points')
  t.true(a.eq(new Point({ x: 12, y: 5 })), 'from object')
  t.true(a.up.eq(new Point(12, 4)), 'up is -y')
  t.true(a.down.eq(new Point(12, 6)), 'down is +y')
  t.true(a.left.eq(new Point(11, 5)), 'left is -x')
  t.true(a.right.eq(new Point(13, 5)), 'right is +x')
  t.true(a.up.right.down.left.eq(a), 'movements can be chained')
  const surround = a.around
  t.true(Array.isArray(surround), 'around returns an array')
  t.is(surround.length, 4, 'around returns 4 items')
  t.true(surround[0].eq(new Point(12, 4)), 'down is first')
})

test('compass points', (t) => {
  const p = new Point(0, 0)
  t.true(p.dir('up').eq(new Point(0, -1)), 'up')
  t.true(p.dir('down').eq(new Point(0, 1)), 'up')
  t.true(p.dir('left').eq(new Point(-1, 0)), 'up')
  t.true(p.dir('right').eq(new Point(1, 0)), 'up')
})

test('distant points', (t) => {
  const pointA = new Point(0, 0)
  const pointB = new Point(3, -4)
  t.is(pointA.distX(pointB), 3, 'horizontal distance')
  t.is(pointA.distY(pointB), 4, 'vertical distance')
  t.is(pointA.distance(pointB), 5, 'thanks, pythagoras')
})

test('talking points', (t) => {
  const p = new Point(1, 2)
  t.is(p.toString(), 'Point(1,2)', 'toString')
  t.deepEqual(p.toJSON(), { x: 1, y: 2 }, 'toJSON')
  t.is(inspect(p), 'Point(1,2)', 'inspect')
})

test('points of constraint', (t) => {
  const upperLeft = new Point(-1, -1)
  const lowerRight = new Point(1, 1)
  const zero = new Point(0, 0)
  t.true(zero.in(upperLeft, lowerRight), 'fully inside')
  t.true(zero.in(lowerRight, upperLeft), 'still inside')
  t.false(new Point(5, 5).in(upperLeft, lowerRight), 'fully outside the box')
  for (const point of zero.around) {
    t.true(point.in(upperLeft, lowerRight), 'along the edge')
  }
})

test('points of contention', (t) => {
  t.throws(
    () => {
      // @ts-expect-error
      log(new Point())
    },
    { instanceOf: SyntaxError },
    'no params',
  )
  t.throws(
    () => {
      log(new Point(12))
    },
    { instanceOf: SyntaxError },
    'only x',
  )
  t.throws(
    () => {
      // @ts-expect-error
      log(new Point('1', '4'))
    },
    { instanceOf: SyntaxError },
    'strings',
  )
  t.throws(
    () => {
      const p = new Point(1, 2)
      p.x = 17
    },
    { instanceOf: ReferenceError },
    'immutability of x',
  )
  t.throws(
    () => {
      const p = new Point(5, 3)
      p.y = 42
    },
    { instanceOf: ReferenceError },
    'immutability of y',
  )
  t.throws(
    () => {
      const p = new Point(2, 0)
      // @ts-expect-error
      p.dir()
    },
    { instanceOf: SyntaxError },
    'stringless dir',
  )
  t.throws(
    () => {
      const p = new Point(7, -6)
      p.dir('banana')
    },
    { instanceOf: SyntaxError },
    'invalid dir',
  )
})
