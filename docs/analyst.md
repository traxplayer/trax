# analyst.js API documentation

This module provides additional analysis of Trax games, which can be used in the
CLI and for bots.

## Example usage

```javascript
import { Trax } from '@slugbugblue/trax'
import { analyze, suggest } from '@slugbugblue/trax/analyst'

const trax = new Trax('trax', '@0/ @1/ B0\\')

const analysis = analyze(trax)

console.log(JSON.stringify(analysis.edge, null, 2))
console.log(JSON.stringify(analysis.threats, null, 2))
console.log(JSON.stringify(analysis.scores, null, 2))

const suggestion = suggest(trax)

console.log(JSON.stringify(suggestion.options, null, 2))

trax.play(suggestion.pick.move)
```

## API

### Analysis class

Available either as `Analysis` to be used with `new`, or it can also be returned
using the `analyze` shortcut function.

Provides important properties and functions to understand the current state of a
Trax game.

#### constructor

##### `new Analysis(game)`

##### `analyze(game)`

Provide a Trax `game`, ideally with at least one move and not yet completed.

#### computed properties

##### `game`

The Trax object that represents the position originally analyzed.

##### `edge`

An object with two properties, `b` and `w`, which are string representations of
the traced edge, with one character per space, along with an array named `edge`
which contains the full internal details of each edge space. This object is
tightly tied to the internal representation of the edge tracing algorithm and as
such is subject to change in future iterations.

Edge tracing begins in the top left of the tiles (ie, space A0 or the nearest
playable space to it) and walks clockwise around the perimeter of the played
tiles, recording information about the lines that lead to each space. Note that
the edge spaces are just outside the existing tiles, not on the tiles
themselves. The following keys are present in each edge space representation:

- `x` and `y`: The location of the edge space, which can be directly used to
  create a `Point` for use in any function that takes a location.
- `c`: The color of the line that leads into this space. This can be `b` or `w`
  or `l` or `r`, the latter two representing both black and white lines ("left
  turn") or neither ("right turn").
- `t`: The type of space, which is `n` for a normal space, `c` for a space
  inside a cave that has limits on which tiles can be legally played in it, or
  `x` if there are no legal moves available for this space, which can happen
  both in caves or in 8x8 Trax when the game size limits have been reached.
- `w` and `b`: True if the next line of the same color is the other end of this
  line.
- `nw` and `nb`: The number of the white and/or black lines coming to this
  space, which can then be used to find the other end of the line.
- `pb` and `pw`: True if the next line of the same color is part of a
  connectable pair of lines with this color.
- `zb` and `zw`: The character used in the black or white string representation
  of this space.
- `idx`: The index of this edge space in the edge array.

For the edge string, the following characters are used as a hopefully useful
shorthand summary of the tiles surrounding the space. To make regex pattern
matching simpler, the black string is constructed by first swapping the colors,
so that it appears as though all black tiles are white tiles.

- `b`: a black line enters this space.
- `w`: a white line enters this space.
- `l`: both a black and a white line enter this space.
- `a`: a white line enters this space, and the next white line in the edge is
  the other end of this line.
- `c`: a white line enters this space, and the next white line in the edge forms
  a connectable pair with this line.
- `m`: both a black and a white line enter this space, and the next white line
  in the edge is the other end of the white line.
- `p`: both a black and a white line enter this space, and the next while line
  in the edge forms a connectable pair with the white line.
- `r`: no lines enter this space, but it represents a right-corner in the edge.

##### `threats`

Analyzes the existing threats for the current game position. Returns an object
with both `b` and `w` keys, where each is an object with zero or more "depth"
keys. Threats of depth 0 are corners or connectable-pairs. Threats of depth 1
are immediate attacks. Threats of depth 2 are Ls. etc. Note that the threats are
not checked for faultiness at this time. In other words, there may be a faulty L
listed as a threat even if it is not currently possible to activate the L
successfully.

The "depth" keys are arrays of the threats at that level, which means that a
simple count of the threats at each level can give a good first guess at the
"score" of the current position. Each element of the array is an object with the
following keys:

- `threat`: The threat pattern as it was fed into the threats database. This is
  a simple edge string.
- `match`: The portion of the actual edge string that matched the pattern, which
  may or may not be exactly the same as the pattern.
- `at`: The index of the edge string at which the pattern match began.
- `value`: The multiplier for this threat. Most often this is `1`, but some
  threats can be re-formed, and this will be `2` in those cases.

##### `faulty`

An object with two keys, `w` and `b`, each of which is a list of threats that
were found to be fauly.

Example data:

```javascript
{
  w: [{ threat: 'Arw?!pW', match: 'arwpa', at: 1, level: 2, value: 1 }],
  b: [],
}
```

##### `score`

Computes a rough "score" of the current position using the list of threats. A
positive score means that the player whose turn it is has a potential advantage,
with the larger the number of higher the advantage. A negative score indicates a
disadvantage.

##### `scores`

Returns the same score as above, but packaged into an object with both a `b` and
`w` key, to more easily grab the score of a particular player without regard to
whose turn it is. Note that `b` will not necessarily be equal to `w` times -1
because the scores can change depending on who has the initiative.

#### methods

##### `count.w(level)`

Returns the number of threats for white at the given level.

##### `count.b(level)`

Returns the number of threats for black at the given level.

##### `inCave(loc)`

For any given location, returns true if that location is found within a cave.

##### `spaceType(loc)`

For any given location, determine the type of playable space. Returns one of:

- `n`: This location can be played in normally.
- `x`: A move is not valid in this location.
- `c`: This location is in a cave, but otherwise has no restrictions on its
  play.
- `C`: This location is in a cave, and certain plays at this position result in
  illegal moves.

### Suggestion class

Accessible from `suggest(game)`, this class is returned with several useful
pieces of information.

#### properties

##### `all`

A list of all the moves analyzed, ordered by highest scoring move first.

Each move is an object with the following properties:

- `move`: the notation of the move
- `score`: the score of the move
- `analysis`: the analysis object of the position for this move

##### `options`

A smaller subset of the `all` list, with only the moves that are worth
considering.

##### `pick`

A random selection of one of the `options`.

##### `ms`

The number of milliseconds spent performing the analyses required to create the
suggestion.

#### computed properties

- `best`: quick access to the top-scoring move
- `analyzed`: quick access to a count of the number of moves analyzed

## License

Copyright 2022-2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
