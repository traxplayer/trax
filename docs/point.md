# point.js API documentation

This module provides a class structure to work with `x`, `y` locations as a
single point.

Since I don't need it to be super fancy, there isn't a lot of extra
functionality here.

## Example usage

```javascript
import { Point } from '@slugbugblue/trax/point.js'

const zero = new Point(0, 0)

const up = zero.up
console.log('up:', up.x, up.y) // up: 0 -1

const rt = zero.right
console.log('rt:', rt) // rt: Point(1,0)

const up2 = zero.dir('up')
console.log('up === up2 ?', up.eq(up2)) // up === up2 ? true

// distances
console.log(zero.distX(up), zero.distX(rt)) // 0 1
console.log(up.distX(rt), up.distY(rt)) // 0 0
console.log(zero.distance(up), zero.distance(rt)) // 1 1
console.log(up.distance(rt)) // 1.4142135623730951
```

## API

### Point class

A Point instance is a representation of an immutable point in two dimensional
space, represented by an `x` and a `y` component. Since this implementation was
created for game boards, we assume that all points are integers, though that is
not a requirement, and the math will work equally well on non-integers.

#### constructor

##### `new Point(fromPoint)`

##### `new Point(x, y)`

You can create a point from either a point-like object (ie, any object with both
an `x` and a `y` key with numeric values), or from individual `x` and `y`
numbers. Once a point is created, it cannot be changed; however, you can use its
functions to return other points relative to it.

#### properties

##### `x` and `y` immutable properties

You can return the individual `x` or `y` property of a point by accessing it
directly. Attempts to change these properties will fail.

##### `up`, `down`, `left`, `right` calculated properties

Since points are immutable, to move a point, you need to create a new point.
These calculated properties provide quick access to cardinal integer neighboring
points.

##### `around` calculated property

Or, get an array of all four cardinal points around the existing point.

#### methods

##### `dir(direction)`

Pass in a string named direction (ie, one of `up`, `down`, `left`, or `right`)
to get a point one integer away from the existing point in the direction
indicated. This does the exact same thing as the calculated properties with
these names, but it can be done using a variable instead.

##### `distX(from)`, `distY(from)`

Returns the difference between the `x` or `y` properties of two points. This
will always be represented by zero or a positive number.

##### `distance(from)`

Returns the distance between two points. This will never be a negative number.

##### `eq(to)`

Returns `true` if the given point or point-like object is at the same location
as the existing point.

##### `in(a, b)`

Pass in two points (or point-like objects) to define a rectangular bounding box.
Each point represents opposite corners of the box.

Returns `true` if the calling point is located inside the box or along any of
its edges (ie, both bounding points are considered "inside").

#### Plays well with others

The Point class also includes functionality to provide useful default results
when used in other contexts.

##### `toString()`

Returns a string representation of the point in the format `Point(x,y)`. This
allows simple logging calls with `console.log(point)`.

##### `toJSON()`

JSON cannot encode special classes, but this class can be stored as a bare
object with `x` and `y` keys when converted to JSON. Note, though, that when
parsing the resultant JSON string, all Point functionality is not restored by
default. For example:

```javascript
const zero = new Point(0, 0)
const json = JSON.stringify(zero) // '{"x":0,"y":0}'
const jsonzero = JSON.parse(json) // { x: 0, y: 0 }

console.log(zero.eq(jsonzero)) // true
jsonzero.eq(zero) // TypeError: jsonzero.eq is not a function

// However, the object can easily be turned back into a Point:
const reconstituted = new Point(jsonzero)
console.log(reconstituted.eq(zero)) // true
```

##### `util.inspect.custom()`

When running in the node command line interpreter, a point will be
pretty-printed using the magic of `util.inspect`.

## License

Copyright 2019-2022 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
