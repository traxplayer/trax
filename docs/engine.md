# engine.js API documentation

This module provides the logic to handle interacting with a Trax game, including
starting a game, making moves in the game, and determining if the game has been
won.

A few other functions/properties are also provided to assist with determining
the game state and what actions can be taken on the game.

## Example usage

```javascript
import { Trax } from '@slugbugblue/trax'

let trax = new Trax()
console.log(trax.possible_moves()) // "[ '@0/', '@0+' ]" -- initial moves

trax.play('@0/')
console.log(trax.turn) // "2" -- it is player 2's turn

trax.play('@1\\')
console.log(trax.color) // "w" -- it is white's turn

trax.play('A0/')
console.log(trax.over) // "true" -- the game is over
console.log(trax.winner) // "1" -- player 1 wins
```

## API

### Trax class

The Trax object can be called as a constructor, but it also has the following
class properties and methods:

- `Trax.variants`: a Set of the different rule sets the engine supports. Try
  `Trax.variants.has(rule)` to see if your favorite rule set is supported. Or
  `Trax.variants.forEach((rule) => { console.log(rule) })` to list them all.

- `Trax.names`: an object of variant rule set names to English names.

- `Trax.point(x, y)`: quick access to the [Point class][point-docs], for
  two-dimensional locations used internally by the Trax engine and which are
  required for some of the lower-level engine functions. Operates as a
  constructor (without `new`) and returns a [Point instance][point-docs].

- `Trax.colorOf(playerNumber)`: Player 1 is white and Player 2 is black. So this
  is a quick function to turn `1` into `'w'` and `2` into `'b'`. Quick and
  dirty.

- `Trax.playerNumber(color)`: Quick and dirty way to turn `'w'` into `1` and
  `'b'` into `2`.

- `Trax.other(color)`: Quick and dirty way to turn `'w'` into `'b'` and
  vice-versa.

- `Trax.encodeCol(columnNumber)`: a function to turn a numeric column number
  into the column letter used in Trax notation. The first column is `A`. The
  empty column just to the left is `@`.

- `Trax.decodeCol(columnLetter)`: turns a column letter back into a number.

#### constructor

```javascript
trax = new Trax(rules, moves, id)
```

- `rules`: a string used to determine which trax variant will be played. If this
  string is missing or is not found in `Trax.variants`, the default rule set of
  `trax` will be used.

- `moves`: an array of moves or a space-separated string of moves to use as the
  starting position of the game board. If missing, the board will be empty. Note
  that if an array is used, each item in the array should be a notation string.
  If a string is used instead, it is permissible to include the move numbers of
  each move as well. As an example, the following are equivalent:

  - `['@0/', '@1+', 'B0\\']`
  - `'1. @0/ 2. @1+ 3. B0\\'`
  - `@0/ @1+ b0\\`

- `id`: an optional string id that will be prepended to the key of each tile.
  This is useful for setting the `id` value of HTML entities, but will probably
  not be very useful for any other situations. You can likely safely ignore
  this. Defaults to `'trax'`.

Returns an instance of the `Trax` class, which contains the following properties
and methods:

### Trax instance

#### properties

- `id`: the string provided to the constructor
- `rules`: which of the `Trax.variants` is being played
- `move`: number, how many moves have been played
- `turn`: number, the player to make the next move
- `over`: boolean, `true` if the game is over
- `left`: number, the left-most column in play
- `right`: number, the right-most column in play
- `top`: number, the top-most row in play
- `bottom`: number, the bottom-most row in play
  - note that each of these four numbers are using the internal coordinates of
    the board rather than the Trax notation coordinates, since the latter
    potentially change after every move
- `notation`: string, the notation of the current state, wrapped at 80 columns
- `tiles`: object, key/value pairs of data for each tile on the board
  - key: a string composed of the game id and location of the tile
  - value: an object containing the following key/value pairs:
    - `type`: a string (`a` - `f`) representing the tile
    - `loc`: an (x, y) Point object with the tile location
    - `id`: the key of this object, provided here for easy cross-reference
    - `move`: the move number this tile was placed on the board
    - `seq`: the order in which this tile was played
- `path`: array, each item is a key of `tiles` involved in the winning path
- `invalid`: boolean, `true` only when a tile played into a cave results in an
  illegal move

Note that while there is no protection on these properties, you should treat
them as read-only, since changing them directly will likely result in
unpredictable and invalid operations by the engine.

#### calculated properties

- `name`: string, a shortcut to get the actual name of the game from its variant
  rule set code
- `count`: number, the total number of tiles currently in play
- `color`: string, the color (`'w'` or `'b'`) of the player whose turn it is
- `gameOver`: boolean, `true` if the game is over
- `winner`: `false` if the game is still in progress, number of the winning
  player otherwise (0 = tie)
- `height`: the number of rows in play
- `width`: the number of columns in play
- `moves`: an array of move notations for the current game
- `icon`: a string containing an encoded representation of the current board
  position, useful for quickly drawing the game board without having to iterate
  through the list of tiles (see [Encoding the icon](#encoding-the-icon) for
  details on the structure of this code)
- `normalized`: the normalized code for this position

#### useful methods

##### `save()`

This method encodes and returns the current state of the game into an object
that can later be restored. You should treat this object as opaque, to be used
only with the `restore()` method. Save a position before attempting tentative or
exploratory moves, so that you can restore the original position without having
to re-play all of the moves to arrive at that position.

`save()` and `restore()` are preferable to saving the current notation and
calling `new Trax()` with that notation, since it avoids all of the calculations
necessary to arrive at the same state.

##### `restore(savedObj)`

This method accepts the returned value of the `save()` method and restores the
previously saved position.

##### `possibleMoves()`

Returns a list of all moves (using game notation) that can be made from the
current position. Since it does not examine the result of forced tiles for each
move, it does not detect illegal cave positions, so there is no guarantee that
each move that can be played will be legal.

##### `play(notation)`

##### `play(moveNumber, notation)`

Attempts to play the given `notation`. If `moveNumber` is provided, add a
safeguard around the move to ensure that the move is being submitted against the
expected position, which can be useful in the context of asynchronous
communication and retries, such as when this engine is being used as the back
end of a web interface. For example, if no moves have been made yet in a game,
this can be called either as `play(1, '@0/')` or `play(1, '@0+')`.

Updates the game instance if the notation is valid for the current position.

Returns an object with at minimum the following key/value pair:

- `valid`: `true` if the move is valid and results in a legal position

If the move is valid, the following will also be part of the returned object:

- `dropped`: a list of the tiles that were played by this move
- `notation`: the resulting notation of the move

##### `playMoves(moves)`

Can be used to enter multiple `moves` at once. Pass in either a string of
space-separated notations or an array of notations. In both cases move numbers
can be optionally included, in which case the moves are not made if the move
numbers do not match the board position. This function tries to be as forgiving
as possible of format.

For example, if a new trax object with zero moves were just created, each of the
following statements would put the game in the same position:

```javascript
trax.playMoves('@0/ A0+ B1/')
trax.playMoves(['@0/', 'a0+', 'B1/'])
trax.playMoves('1 @0/ 2. a0+ 3 b1/')
trax.playMoves([1, '@0/', '2', 'A0+', '3.', 'B1/')
trax.playMoves(['1. @0/', 'a0+', 3, 'b1/')
```

##### `provisionalMove(from, to, via)`

Test to see if a provisionally entered move is a match for the current position,
as determined by the normalized `from` position, playing the move notation
`via`, which must result in the normalized `to` position. Note that normalized
positions match symmetrically identical positions, so it's possible that `via`
isn't the actual move notation required. Symmetrical rotations of the notation
will be examined as well.

Returns `'delete-provisional'` if the provisional move will never be valid in
any future position. Returns `false` if the move doesn't match but may
potentially match a future position. Returns the potentially rotated move
notation if the provisional move was a match.

##### `dropsIcon(drops)`

Returns an `icon` but with the included `drops` encoded distinctly so that they
can be distinguished from other tiles.

#### internal methods you probably don't need to use directly

Since these are not likely to be used directly, the documentation is a little
less complete.

##### `tileId(location)`

Takes a `location` and returns a unique string id for the tile at that location.
The `tiles` property object uses the `tileId()` as a key for the tiles.

##### `addTile(type, location)`

This function is used internally to place a tile of `type` on the board at
`location`. It has no validity checks, since that is handled in other parts of
the interface. It is not recommended to use this directly. It returns the tile
object that it adds to the board.

##### `tileAt(location)`

Checks to see if a tile is at the specified `location`, and returns the tile
type or `undefined`.

##### `validTile(type, location)`

Checks to see if a tile of `type` can be played at the given `location`. Returns
`true` if so. Note that this result is not a guarantee, since it does not check
forced tiles for illegal cave positions.

##### `possibleTiles(location, slash)`

Returns a list of the possibly valid tiles at a given `location`. If the
optional `slash` parameter is included, it limits its search to tiles that also
match that `slash`.

##### `validLocation(location)`

Returns true if a piece can be played at the given `location`. However, this
does not check for illegal cave positions.

##### `possibleLocations()`

Returns a list of all locations where a tile may be played. Does not check for
illegal cave positions.

##### `forcedMoves(location)`

After a tile is placed at `location`, play all the forced moves that result,
adding them to the board as well. Returns the list of all tile objects (as
returned from `addTile()`). It is during this function when illegal cave moves
are detected. When that happens, the tile at that location will be given a type
of `x`, and the `invalid` property of the Trax instance will be set to `true`,
but the tiles will not be removed. It is up to the calling function to check for
this condition and `restore` a previous position.

##### `notate(type, location)`

Return the game notation when playing a tile of `type` at the given `location`.
This has to be done before the move is played, because if the move expands the
board to the left or upward, the notation will be invalid.

##### `decodeNotation(notation)`

Returns an object with two keys: `type` and `location`.

##### `follow(color, location, from)`

Follows the line of one `color` starting at the tile at the given `location`, in
the direction as if coming `from` a given edge. Returns an object giving the (x,
y) `loc` of the final tile, as well as a `path` array holding the tile id's

##### `findEnds(color, location)`

Uses `follow()` to trace both ends of the given `color` from the given
`location`. Returns an array of two items, each containing an object describing
one of the ends of the line, as returned by `follow()`.

##### `lineWin(locA, locB)`

Given `locA` as the (x, y) location of one end of a line and `locB` as the other
end, does the line constitute a line win? Returns `true` or `false`.

##### `checkWin(tiles)`

Used after a play is made to determine if any of the played `tiles` results in a
win. Updates the instance's `over` and `turn` properties as needed. Returns
nothing.

##### `updateNotation(notation)`

Handles updating the `notation` game instance property using the `notation` of
the current move. Returns nothing.

##### `dropTile(notation, _, tentative)`

##### `dropTile(type, location, tentative)`

Attempts to play a tile given either a `notation` or a `type` and `location`. If
`tentative` is provided and is truthy, the move will not actually be made, but
the return value will be the same as if it were made.

Returns an object with the following key/value pairs:

- `dropped`: a list of the tiles that were played by this move
- `notation`: the resulting notation of the move
- `valid`: `true` if the move is valid and results in a legal position

##### `moveRotations(move)`

Rotates a given `move` around the board to assist with symmetry calculations.
Returns an array of possible symmetrical meanings of the given move.

##### `positionCode(rightToLeft, bottomToTop, rotate, drops)`

This is a helper function to provide a string representation to a position,
applying optional symmetrical operations on it. By default, the position code
returned will be provided using a top to bottom, right to left, unrotated
approach. By setting `rightToLeft`, `topToBottom`, or `rotate` to `true`,
transformations can be applied to the position code to return any of 8 different
codes representing the 8 possible symmetries of any given position. If `drops`
is provided, it should be the array of `dropped` tiles returned by `dropTile()`,
and those tiles will be encoded differently. See
[Encoding the icon](#encoding-the-icon) for details on the encoding.

##### `normalize()`

Returns a normalized position code icon. Every symmetrical position will return
the same normalized code, so you can determine if two different positions are
the same position with only changes in symmetry by calling this on both game
instances and comparing the resulting icon string. While this code should be
treated as if opaque, you can read about the way it is created from an
[encoded position code](#encoding-the-icon) in the section on
[normalizing the position code](#normalizing-a-position).

## Trax tiles

There isn't an inherent order to the way in which Trax tiles are presented.
However, to allow for some sort of ease of encoding, the Trax module names each
tile with a lower-case letter from `a` to `f` as shown in the following
ASCII-art representations of each tile. The `##` characters are used to
represent black.

```
  Trax tiles
      a         b         c         d        e         f
  ________  ________  ________  ________  _______  ________
  |__##  |  |__##__|  |  ##__|  |_/ |  |  | | | |  |  | \_|
  |_ \###|  |______|  |###/ _|  |__/###|  |#| |#|  |###\__|
  |_\_|__|  |__##__|  |__|_/_|  |__##__|  |_|_|_|  |__##__|

  (white wins / black wins)
    g  m      h  n      i  o      j  p      k q      l  r
```

### Encoding the icon

The `icon` computed property encodes the position of the board by replacing each
tile on the board with its corresponding letter as described immediately above.
In cases where the tile in question is part of a winning line or loop, the
character for that tile is increased by 6 letters for a white win, or by 12
letters for a black win. In addition, when requested, the icon can also include
information about which tiles were placed in the last move, and those tiles are
represented by their upper-case counterparts.

In the event that a given move results in an illegal position, one of the tiles
that make up the illegal position will be replaced by an `x` instead of one of
the listed tiles. This signifies that no possible tile can meet the forced moves
rules of that location. Only one of the tiles will be so marked, even though it
requires several contributing tiles to create a situation where a move is
illegal.

Spaces are encoded into the icon using single-digit numbers. `0` means there is
one space. `9` would indicate there are 10 spaces. Spaces at the left or in
between tiles are encoded. Empty spaces to the right of all tiles are ignored.

A colon is used to separate rows.

### Normalizing a position

A normalized position is one where a single position code is chosen such that
symmetrical positions will be represented by a single position code.

Different from a standard position code, a normalized position code includes the
addition of a leading character indicating the state of the game. `W` means
either that it is white's turn or that white has won the game. `B` indicates
that black is to play or has already won. `T` means that the game has ended in a
tie.

## License

Copyright 2019-2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

[point-docs]: point.md
