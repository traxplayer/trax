# lru.js API documentation

This module provides a simple Least Recently Used cache.

Internally, this uses a Map, which give us insertion-ordering for free. The
first item in the list is the oldest. The last item is the most recently set or
accessed. Most of the base functionality of the Map are replicated.

Since I don't need it to be super fancy, there isn't a lot of extra
functionality here.

## Example usage

```javascript
import { LRU } from '@slugbugblue/trax/lru.js'

const cache = new LRU(100)

cache.set('a', 1)
cache.set('b', 2)

// Get an item from the cache
console.log(cache.get('a')) // 1

// Accessing a key makes it the most recent:
console.log(cache.keys()) // [Map Iterator] { 'b', 'a' }
console.log(cache.hits) // 1

console.log(cache.get('z')) // undefined
console.log(cache.misses) // 1

console.log(cache.size) // 2
console.log(cache.capacity) // 100
```

## API

### LRU class

An LRU cache is a way to remember a limited set of items, with the oldest
expiring when the capacity is reached.

#### constructor

##### `new LRU(capacity)`

Create the cache by specifying the maximum number of items it should hold.

#### computed properties

##### `capacity`

The maximum number of items the cache can hold. This cannot be changed after the
cache creation.

##### `size`

The number of items currently stored in the cache.

##### `hits`

The number of times a `get` call has found an item in the cache.

##### `misses`

The number of times a `get` call has not found an item in the cache.

##### `expired`

The number of times an item has been removed from the cache.

#### methods

##### `get(key)`

Retrieve an item from the cache. If the item is found in the cache, `hits` will
be updated, the item will be marked as the most-recently-used item, and the
value will be returned. If the item is not present in the cache, `misses` will
be updated, and `undefined` will be returned.

Note that if you had stored `undefined` as the value in the cache, the return of
`undefined` doesn't necessarily represent a cache miss.

##### `peek(key)`

Retrieve an item from the cache, if present, but without updating any of the
LRU-specific information about the retrieval attempt. The item's age will not be
updated. If the item is not present in the cache, `undefined` will be returned.

##### `set(key, value)`

Store an item in the cache. If the item is already present in the cache, it will
be replaced and marked as the most-recently-used item.

##### `delete(key)`

Remove an item from the cache.

##### `clear()`

Clears all items and statistics from the cache. The maximum capacity will not
change.

##### `entries()`

Returns an iterator that operates over all the \[key, value\] pairs stored in
the cache. The entries will be in order from oldest to newest.

##### `has(key)`

Returns true if a key is found in the cache, false otherwise.

##### `keys()`

Returns an iterator that operates over all the keys stored in the cache. The
keys will be returned oldest first, newest last.

##### `values()`

Returns an iterator that operates over all the values stored in the cache. The
values will be returned oldest first, newest last.

#### Plays well with others

The LRU class also includes functionality to provide useful default results when
used in other contexts.

##### `toString()`

Returns a string representation of the cache in the format `LRU(75 of 1000)`.
This allows simple logging calls with `console.log(cache)`.

##### `util.inspect.custom()`

When running in the node command line interpreter, an LRU cache instance will be
pretty-printed using the magic of `util.inspect`.

## License

Copyright 2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
