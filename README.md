# Trax

This project aims to provide tools for working with Trax games in Javascript.

- Trax game-playing engine
- Position analysis engine
- Over 100 puzzles to help hone your Trax skills
- Command-line interface for ease of use

## Description

Trax is a two-player boardless board game invented by David Smith. For more
information about Trax, including its [rules][traxrules] and
[history][traxhistory], see the official website at [traxgame.com][traxgame].

The goal of this javascript project is to allow Trax to be played
programmatically. It can be used as an engine in a web browser, for example at
[slugbugblue.com][sbb]; or in nodejs, for example by using the `trax` CLI
provided as part of this project.

The Trax engine now enjoys automated testing with 100% code coverage. Any future
changes to the engine should meet the same standard.

## Installation

Installation can be done in one of three ways, each for a slightly different
purpose.

If you want to use this code in your own javascript project:

```bash
npm install @slugbugblue/trax
```

If you want to use the trax CLI on your computer:

```bash
npm install -g @slugbugblue/trax
```

If you don't have node installed and want to use the CLI as a docker container:

```bash
git clone https://gitlab.com/slugbugblue/trax.git ~/trax
docker build -t trax ~/trax
```

## CLI Usage

The command-line interface is provided as `trax`, available in your path if
installed globally, or in the `node_modules/.bin` directory and runnable using
`npx trax` if installed locally.

> If you installed by creating a Docker image, use the following command to
> launch the CLI:
>
> - `docker run --name trax -itv $HOME/trax:/mnt/data --rm trax`
>
> Since that's a bit much to type, it's recommended to create an alias in one of
> your startup scripts:
>
> - `alias trax="docker run --name trax -itv $HOME/trax:/mnt/data --rm trax"`
>
> Then you can launch the docker container simply by typing in `trax`, the same
> as if it were installed globally.

The CLI can be used interactively by running the command with no parameters or
by passing the `--interactive` or `-i` flag. When run interactively, you can
avoid worries about the need to escape certain characters from the shell, and it
can be easier to run multiple commands back to back.

Help is available for the CLI through the use of the `help` command. Type
`trax help` (or `npx trax help` with a local installation, or simply `help` if
run interactively) to see a list of the available commands, and `trax help play`
to see information about the `play` command. Aliases and abbreviations are
provided to minimize typing and to try to avoid the requirement of memorizing
the commands. For example, the `list` command has an alias of `ls`, and the
`select` command can be activated using `#`.

When you first launch the CLI, you can create a new game using the `new`
command, and then enter moves for that game using `play`. Since `play` is likely
to be the most common command used, you can simply omit the command name. For
example, at the interactive prompt, entering `@0/` is the same as if you had
entered `play 1. @0/`.

Multiple games can be started at once, and the CLI will remember the currently
active game and apply future actions to that game. Each game will retain its
`#id` number until it is deleted, and you can use that number to switch between
games. For example, to make game \#1 active, enter `select 1` or `select #1` or
simply `#1` at the interactive prompt.

For ease of working at the command line, the symbols used as the last character
of the Trax notation can optionally be replaced with letters. Use `s` for slash
symbol (`/`), `b` for backslash (`\`), and `p` for plus (`+`). While there is no
need to escape at the interactive prompt, these substitutions will be avilable
there as well.

### Examples

```text
# Start a new Trax game with Chad playing as white
> trax new Chad vs

# Start a new Loop Trax game with Chad playing as black
> trax new loop vs Chad

# Start a new 8x8 Trax game with Chad as white and Another Player as black
> trax new 8x8 Chad vs Another Player

# Switch back to the first game. Note the quoting required at the command line.
# This would not be necessary when running at the interactive prompt.
> trax '#1'

# Play two moves, using full notation
> trax play 1. @0/ 2. A0/

# Two more moves, using short notation and symbol substitution
# B2/ => b2s and A0\ => a0b
> trax b2s a0b

# See the status of all games
> trax ls
```

### Environment variables

The CLI uses standard Windows and Linux [XDG][xdg] file locations as provided by
the [env-paths library][env-path], with current uses being user-specific
`config` (`XDG_CONFIG_HOME`) and `data` (`XDG_DATA_HOME`) folders.

In addition, the CLI recognizes several environment variables to determine the
glyphs to use in the user interface. Setting either `NERDFONT` or `NERDFONTS` to
any non-empty string will use characters from [Nerdfonts][nerfont]. Otherwise,
any of `POWERLINE`, `P9K_TTY`, or `P9K_SSH` will use characters from [Powerline
fonts][powerline].

## API Usage

### Trax engine

The main Trax engine is provided in `engine.js`, which can be used as follows:

```javascript
import { Trax } from '@slugbugblue/trax'

// default constructor gives you a Trax game
let trax = new Trax()
trax.play('@0/')

// but you can also select Loop Trax
let loops = new Trax('traxloop')

// or 8x8 Trax
let tiny = new Trax('trax8')

// and the constructor lets you start with an initial set of moves
let puzzle = new Trax('trax', '@0/ a0\\ @2/')
puzzle.play('@2\\')
```

For specifics, see the [engine.js documentation][docs-engine].

### Analysis

The analysis engine is provided in `analyst.js`, and it can be used as follows:

```javascript
import { Trax } from '@slugbugblue/trax'
import { analyze } from '@slugbugblue/trax/analyst'

let trax = new Trax('trax', '@0/ @1\\')

let analysis = analyze(trax)

console.log('Edge analysis:', analysis.edge)
console.log('Threat analysis:', analysis.threats)
console.log('Score analysis:', analysis.scores)
```

For additional information, refer to the [analyst.js
documentation][docs-analyst].

### Threats database

The threats database is currently hand-coded in `threats.js`, so refer to that
file if you want to try to update the analysis engine's understanding of
threats.

### Puzzles database

The puzzles database is provided in `puzzles.js`, and contains puzzles that I've
been using to test the analysis engine, publicly available puzzles from [the
traxgame.com puzzles page][traxpuzzles], and [puzzles curated][puzzlebook] by
[Martin M. S. Pedersen][traxplayer].

## Roadmap

- Improve the threats database / analysis engine.
  - Bot should be able to win all puzzles.
  - Analysis should recognize line threats.
- Add more puzzles.

## Support

This project is built by [Chad Transtrum][ctrans] for [slugbugblue.com][sbb].
Issues can be opened on the [gitlab project page][repo].

## Contributing

[Contributions are welcome][contributing]. Ideally in the form of pull requests,
but feel free to open an issue with a bug report or a suggestion as well.

## Acknowledgments

Many thanks to the late David Smith for coming up with the idea of Trax and for
his generosity in allowing me to add the game to [GoldToken.com][goldtoken].

Thanks to his widow Colleen Foley-Smith for extending that courtesy to allow me
to include Trax on [slugbugblue.com][sbb] as well.

I would also like to express my huge heartfelt appreciation to the magnanimous
[Donald G. Bailey][dgb], who, through his excellent book [Trax Strategy for
Beginners][traxbook], taught me the basics (and more!) of Trax. I found him
always ready to share his insights on the mechanics of the game, as well as
various approaches to encoding its complexities. His undeserved kindness and
infinite patience in indulging my many questions can never be repaid.

Thanks to [Martin M. S. Pedersen][traxplayer] for contributing ideas, code, and
puzzles. He's been [curating Trax puzzles][puzzlebook] and [coding solutions for
Trax][gnutrax] for over two decades, and brings a lot of expertise.

## License

Copyright 2019-2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

## Trax rules copyright

In the United States, game mechanics are not eligible for copyright protection;
however, the specific wording of game rules does fall under copyright law.

The official rules for Trax are found at http://traxgame.com/about_rules.php.

As Trax is a proprietary game, these rules are the intellectual property of
David Smith and heirs, and are not to be used without permission.

[contributing]: CONTRIBUTING.md
[ctrans]: mailto:chad@transtrum.net
[dgb]: mailto:donald@traxgame.com
[docs-engine]: docs/engine.md
[docs-analyst]: docs/analyst.md
[env-path]: https://www.npmjs.com/package/env-paths
[gnutrax]: https://gnutrax.com
[goldtoken]: https://goldtoken.com/
[nerdfont]: https://nerdfonts.com/
[powerline]: https://github.com/powerline/fonts
[puzzlebook]: https://gnutrax.com/book
[repo]: https://gitlab.com/slugbugblue/trax
[sbb]: https://slugbugblue.com/
[traxbook]: http://traxgame.com/shop_book.php
[traxgame]: http://traxgame.com/
[traxhistory]: http://www.traxgame.com/about_history.php
[traxplayer]: mailto:traxplayer@gmail.com
[traxpuzzles]: http://www.traxgame.com/games_puzzles.php
[traxrules]: http://www.traxgame.com/about_rules.php
[xdg]:
  https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
